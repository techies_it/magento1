<?php
/**
 * Techies_InsurancePlan extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Techies
 * @package        Techies_InsurancePlan
 * @copyright      Copyright (c) 2016
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Insurance Plan edit form tab
 *
 * @category    Techies
 * @package     Techies_InsurancePlan
 * @author      Bal Singh
 */
class Techies_InsurancePlan_Block_Adminhtml_Insuranceplan_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return Techies_InsurancePlan_Block_Adminhtml_Insuranceplan_Edit_Tab_Form
     * @author Bal Singh
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('insuranceplan_');
        $form->setFieldNameSuffix('insuranceplan');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'insuranceplan_form',
            array('legend' => Mage::helper('techies_insuranceplan')->__('Insurance Plan'))
        );

        $fieldset->addField(
            'name',
            'text',
            array(
                'label' => Mage::helper('techies_insuranceplan')->__('Name'),
                'name'  => 'name',
            'required'  => true,
            'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'website',
            'text',
            array(
                'label' => Mage::helper('techies_insuranceplan')->__('website'),
                'name'  => 'website',

           )
        );
        $fieldset->addField(
            'status',
            'select',
            array(
                'label'  => Mage::helper('techies_insuranceplan')->__('Status'),
                'name'   => 'status',
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('techies_insuranceplan')->__('Enabled'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('techies_insuranceplan')->__('Disabled'),
                    ),
                ),
            )
        );
        $fieldset->addField(
            'claim',
            'select',
            array(
                'label'  => Mage::helper('techies_insuranceplan')->__('Claim'),
                'name'   => 'claim',
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('techies_insuranceplan')->__('Yes'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('techies_insuranceplan')->__('No'),
                    ),
                ),
            )
        );
        $fieldset->addField(
            'search_tag',
            'editor',
            array(
                'label' => Mage::helper('techies_dme')->__('Search Tag'),
                'name'  => 'search_tag'
           )
        );
        $fieldset->addField(
            'description',
            'editor',
            array(
                'label' => Mage::helper('techies_dme')->__('Description'),
                'name'  => 'description'
           )
        );
        
        $formValues = Mage::registry('current_insuranceplan')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getInsuranceplanData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getInsuranceplanData());
            Mage::getSingleton('adminhtml/session')->setInsuranceplanData(null);
        } elseif (Mage::registry('current_insuranceplan')) {
            $formValues = array_merge($formValues, Mage::registry('current_insuranceplan')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
