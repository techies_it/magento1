<?php
/**
 * Techies_InsurancePlan extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Techies
 * @package        Techies_InsurancePlan
 * @copyright      Copyright (c) 2016
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Insurance Plan admin edit tabs
 *
 * @category    Techies
 * @package     Techies_InsurancePlan
 * @author      Bal Singh
 */
class Techies_InsurancePlan_Block_Adminhtml_Insuranceplan_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Bal Singh
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('insuranceplan_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('techies_insuranceplan')->__('Insurance Plan'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return Techies_InsurancePlan_Block_Adminhtml_Insuranceplan_Edit_Tabs
     * @author Bal Singh
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_insuranceplan',
            array(
                'label'   => Mage::helper('techies_insuranceplan')->__('Insurance Plan'),
                'title'   => Mage::helper('techies_insuranceplan')->__('Insurance Plan'),
                'content' => $this->getLayout()->createBlock(
                    'techies_insuranceplan/adminhtml_insuranceplan_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve insurance plan entity
     *
     * @access public
     * @return Techies_InsurancePlan_Model_Insuranceplan
     * @author Bal Singh
     */
    public function getInsuranceplan()
    {
        return Mage::registry('current_insuranceplan');
    }
}
