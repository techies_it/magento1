<?php
/**
 * Techies_InsurancePlan extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Techies
 * @package        Techies_InsurancePlan
 * @copyright      Copyright (c) 2016
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Insurance Plan admin edit form
 *
 * @category    Techies
 * @package     Techies_InsurancePlan
 * @author      Bal Singh
 */
class Techies_InsurancePlan_Block_Adminhtml_Insuranceplan_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'techies_insuranceplan';
        $this->_controller = 'adminhtml_insuranceplan';
        $this->_updateButton(
            'save',
            'label',
            Mage::helper('techies_insuranceplan')->__('Save Insurance Plan')
        );
        $this->_updateButton(
            'delete',
            'label',
            Mage::helper('techies_insuranceplan')->__('Delete Insurance Plan')
        );
        $this->_addButton(
            'saveandcontinue',
            array(
                'label'   => Mage::helper('techies_insuranceplan')->__('Save And Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class'   => 'save',
            ),
            -100
        );
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * get the edit form header
     *
     * @access public
     * @return string
     * @author Bal Singh
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_insuranceplan') && Mage::registry('current_insuranceplan')->getId()) {
            return Mage::helper('techies_insuranceplan')->__(
                "Edit Insurance Plan '%s'",
                $this->escapeHtml(Mage::registry('current_insuranceplan')->getName())
            );
        } else {
            return Mage::helper('techies_insuranceplan')->__('Add Insurance Plan');
        }
    }
}
