<?php
/**
 * Techies_InsurancePlan extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Techies
 * @package        Techies_InsurancePlan
 * @copyright      Copyright (c) 2016
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Insurance Plan admin grid block
 *
 * @category    Techies
 * @package     Techies_InsurancePlan
 * @author      Bal Singh
 */
class Techies_InsurancePlan_Block_Adminhtml_Insuranceplan_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Bal Singh
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('insuranceplanGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return Techies_InsurancePlan_Block_Adminhtml_Insuranceplan_Grid
     * @author Bal Singh
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('techies_insuranceplan/insuranceplan')
            ->getCollection();
        $collection->getSelect()->joinLeft(array('dmeinsurance'=> $collection->getTable('techies_dmeinsurance/dmeinsurance')), 'dmeinsurance.insurance_plan_id = main_table.entity_id', array('count(dmeinsurance.entity_id) as number_of_dme'));
        $collection->getSelect()->group('main_table.entity_id');
        //echo $collection->getSelect();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    protected function _prepaNumberDmeFilter($collection, $column)
    {
        (int) $filterValue = $column->getFilter()->getValue();

        if (!is_null($filterValue)) {
            $collection->getSelect()->having('count(dmeinsurance.entity_id) = ?', $filterValue);
        }
    }
    /**
     * prepare grid collection
     *
     * @access protected
     * @return Techies_InsurancePlan_Block_Adminhtml_Insuranceplan_Grid
     * @author Bal Singh
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            array(
                'header' => Mage::helper('techies_insuranceplan')->__('Id'),
                'index'  => 'entity_id',
                'type'   => 'number'
            )
        );
        $this->addColumn(
            'name',
            array(
                'header'    => Mage::helper('techies_insuranceplan')->__('Name'),
                'align'     => 'left',
                'index'     => 'name',
            )
        );
        $this->addColumn('number_of_dme', array(
            'header' => Mage::helper('techies_ardoreferral')->__('Number of DME'),
            'index' => 'number_of_dme',
            'filter'    => false,
            //'filter_index'    => 'number_of_dme',
            //'filter_condition_callback'     => array($this, '_prepaNumberDmeFilter')
        ));
        $this->addColumn(
            'status',
            array(
                'header'  => Mage::helper('techies_insuranceplan')->__('Status'),
                'index'   => 'status',
                'type'    => 'options',
                'options' => array(
                    '1' => Mage::helper('techies_insuranceplan')->__('Enabled'),
                    '0' => Mage::helper('techies_insuranceplan')->__('Disabled'),
                )
            )
        );
        $this->addColumn(
            'website',
            array(
                'header' => Mage::helper('techies_insuranceplan')->__('website'),
                'index'  => 'website',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'created_at',
            array(
                'header' => Mage::helper('techies_insuranceplan')->__('Created at'),
                'index'  => 'created_at',
                'width'  => '120px',
                'type'   => 'datetime',
            )
        );
        $this->addColumn(
            'updated_at',
            array(
                'header'    => Mage::helper('techies_insuranceplan')->__('Updated at'),
                'index'     => 'updated_at',
                'width'     => '120px',
                'type'      => 'datetime',
            )
        );
        $this->addColumn(
            'action',
            array(
                'header'  =>  Mage::helper('techies_insuranceplan')->__('Action'),
                'width'   => '100',
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('techies_insuranceplan')->__('Edit'),
                        'url'     => array('base'=> '*/*/edit'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'is_system' => true,
                'sortable'  => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('techies_insuranceplan')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('techies_insuranceplan')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('techies_insuranceplan')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return Techies_InsurancePlan_Block_Adminhtml_Insuranceplan_Grid
     * @author Bal Singh
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('insuranceplan');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label'=> Mage::helper('techies_insuranceplan')->__('Delete'),
                'url'  => $this->getUrl('*/*/massDelete'),
                'confirm'  => Mage::helper('techies_insuranceplan')->__('Are you sure?')
            )
        );
        $this->getMassactionBlock()->addItem(
            'status',
            array(
                'label'      => Mage::helper('techies_insuranceplan')->__('Change status'),
                'url'        => $this->getUrl('*/*/massStatus', array('_current'=>true)),
                'additional' => array(
                    'status' => array(
                        'name'   => 'status',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('techies_insuranceplan')->__('Status'),
                        'values' => array(
                            '1' => Mage::helper('techies_insuranceplan')->__('Enabled'),
                            '0' => Mage::helper('techies_insuranceplan')->__('Disabled'),
                        )
                    )
                )
            )
        );
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param Techies_InsurancePlan_Model_Insuranceplan
     * @return string
     * @author Bal Singh
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Bal Singh
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return Techies_InsurancePlan_Block_Adminhtml_Insuranceplan_Grid
     * @author Bal Singh
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }
}
