<?php
/**
 * Techies_InsurancePlan extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Techies
 * @package        Techies_InsurancePlan
 * @copyright      Copyright (c) 2016
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Insurance Plan admin block
 *
 * @category    Techies
 * @package     Techies_InsurancePlan
 * @author      Bal Singh
 */
class Techies_InsurancePlan_Block_Adminhtml_Insuranceplan extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function __construct()
    {
        $this->_controller         = 'adminhtml_insuranceplan';
        $this->_blockGroup         = 'techies_insuranceplan';
        parent::__construct();
        $this->_headerText         = Mage::helper('techies_insuranceplan')->__('Insurance Plan');
        $this->_updateButton('add', 'label', Mage::helper('techies_insuranceplan')->__('Add Insurance Plan'));

    }
}
