<?php
/**
 * Techies_InsurancePlan extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Techies
 * @package        Techies_InsurancePlan
 * @copyright      Copyright (c) 2016
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Insurance Plan admin controller
 *
 * @category    Techies
 * @package     Techies_InsurancePlan
 * @author      Bal Singh
 */
class Techies_InsurancePlan_Adminhtml_Insuranceplan_InsuranceplanController extends Techies_InsurancePlan_Controller_Adminhtml_InsurancePlan
{
    /**
     * init the insurance plan
     *
     * @access protected
     * @return Techies_InsurancePlan_Model_Insuranceplan
     */
    protected function _initInsuranceplan()
    {
        $insuranceplanId  = (int) $this->getRequest()->getParam('id');
        $insuranceplan    = Mage::getModel('techies_insuranceplan/insuranceplan');
        if ($insuranceplanId) {
            $insuranceplan->load($insuranceplanId);
        }
        Mage::register('current_insuranceplan', $insuranceplan);
        return $insuranceplan;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('techies_insuranceplan')->__('Insurance Plan'))
             ->_title(Mage::helper('techies_insuranceplan')->__('Insurance Plans'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit insurance plan - action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function editAction()
    {
        $insuranceplanId    = $this->getRequest()->getParam('id');
        $insuranceplan      = $this->_initInsuranceplan();
        if ($insuranceplanId && !$insuranceplan->getId()) {
            $this->_getSession()->addError(
                Mage::helper('techies_insuranceplan')->__('This insurance plan no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getInsuranceplanData(true);
        if (!empty($data)) {
            $insuranceplan->setData($data);
        }
        Mage::register('insuranceplan_data', $insuranceplan);
        $this->loadLayout();
        $this->_title(Mage::helper('techies_insuranceplan')->__('Insurance Plan'))
             ->_title(Mage::helper('techies_insuranceplan')->__('Insurance Plans'));
        if ($insuranceplan->getId()) {
            $this->_title($insuranceplan->getName());
        } else {
            $this->_title(Mage::helper('techies_insuranceplan')->__('Add insurance plan'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new insurance plan action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save insurance plan - action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('insuranceplan')) {
            try {
                $insuranceplan = $this->_initInsuranceplan();
                $insuranceplan->addData($data);
                $insuranceplan->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('techies_insuranceplan')->__('Insurance Plan was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $insuranceplan->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setInsuranceplanData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('techies_insuranceplan')->__('There was a problem saving the insurance plan.')
                );
                Mage::getSingleton('adminhtml/session')->setInsuranceplanData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('techies_insuranceplan')->__('Unable to find insurance plan to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete insurance plan - action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $insuranceplan = Mage::getModel('techies_insuranceplan/insuranceplan');
                $insuranceplan->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('techies_insuranceplan')->__('Insurance Plan was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('techies_insuranceplan')->__('There was an error deleting insurance plan.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('techies_insuranceplan')->__('Could not find insurance plan to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete insurance plan - action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function massDeleteAction()
    {
        $insuranceplanIds = $this->getRequest()->getParam('insuranceplan');
        if (!is_array($insuranceplanIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('techies_insuranceplan')->__('Please select insurance plans to delete.')
            );
        } else {
            try {
                foreach ($insuranceplanIds as $insuranceplanId) {
                    $insuranceplan = Mage::getModel('techies_insuranceplan/insuranceplan');
                    $insuranceplan->setId($insuranceplanId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('techies_insuranceplan')->__('Total of %d insurance plans were successfully deleted.', count($insuranceplanIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('techies_insuranceplan')->__('There was an error deleting insurance plans.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function massStatusAction()
    {
        $insuranceplanIds = $this->getRequest()->getParam('insuranceplan');
        if (!is_array($insuranceplanIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('techies_insuranceplan')->__('Please select insurance plans.')
            );
        } else {
            try {
                foreach ($insuranceplanIds as $insuranceplanId) {
                $insuranceplan = Mage::getSingleton('techies_insuranceplan/insuranceplan')->load($insuranceplanId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d insurance plans were successfully updated.', count($insuranceplanIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('techies_insuranceplan')->__('There was an error updating insurance plans.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function exportCsvAction()
    {
        $fileName   = 'insuranceplan.csv';
        $content    = $this->getLayout()->createBlock('techies_insuranceplan/adminhtml_insuranceplan_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function exportExcelAction()
    {
        $fileName   = 'insuranceplan.xls';
        $content    = $this->getLayout()->createBlock('techies_insuranceplan/adminhtml_insuranceplan_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function exportXmlAction()
    {
        $fileName   = 'insuranceplan.xml';
        $content    = $this->getLayout()->createBlock('techies_insuranceplan/adminhtml_insuranceplan_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Bal Singh
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('techies_insuranceplan/insuranceplan');
    }
}
