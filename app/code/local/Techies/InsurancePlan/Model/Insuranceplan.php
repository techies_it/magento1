<?php
/**
 * Techies_InsurancePlan extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Techies
 * @package        Techies_InsurancePlan
 * @copyright      Copyright (c) 2016
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Insurance Plan model
 *
 * @category    Techies
 * @package     Techies_InsurancePlan
 * @author      Bal Singh
 */
class Techies_InsurancePlan_Model_Insuranceplan extends Mage_Core_Model_Abstract
{
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'techies_insuranceplan_insuranceplan';
    const CACHE_TAG = 'techies_insuranceplan_insuranceplan';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'techies_insuranceplan_insuranceplan';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'insuranceplan';

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('techies_insuranceplan/insuranceplan');
    }

    /**
     * before save insurance plan
     *
     * @access protected
     * @return Techies_InsurancePlan_Model_Insuranceplan
     * @author Bal Singh
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()) {
            $this->setCreatedAt($now);
        }
        $this->setUpdatedAt($now);
        return $this;
    }

    /**
     * save insurance plan relation
     *
     * @access public
     * @return Techies_InsurancePlan_Model_Insuranceplan
     * @author Bal Singh
     */
    protected function _afterSave()
    {
        return parent::_afterSave();
    }

    /**
     * get default values
     *
     * @access public
     * @return array
     * @author Bal Singh
     */
    public function getDefaultValues()
    {
        $values = array();
        $values['status'] = 1;
        return $values;
    }
    
}
