<?php
$installer = $this;
$installer->startSetup();
$installer->getConnection()->addColumn($installer->getTable('techies_insuranceplan/insuranceplan'), 'description', array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'nullable' => TRUE,
            'comment' => 'Description'
        ));
$installer->endSetup();
?>