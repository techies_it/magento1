<?php
$installer = $this;
$installer->startSetup();
$installer->getConnection()->addColumn($installer->getTable('techies_insuranceplan/insuranceplan'), 'search_tag', array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'nullable' => TRUE,
            'comment' => 'Search Tag'
        ));
$installer->getConnection()->addColumn($installer->getTable('techies_insuranceplan/insuranceplan'), 'claim', array(
            'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
            'nullable' => TRUE,
            'comment' => 'claim'
        ));
$installer->endSetup();
?>