<?php
/**
 * Techies_InsurancePlan extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Techies
 * @package        Techies_InsurancePlan
 * @copyright      Copyright (c) 2016
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * InsurancePlan module install script
 *
 * @category    Techies
 * @package     Techies_InsurancePlan
 * @author      Bal Singh
 */
$this->startSetup();
$table = $this->getConnection()
    ->newTable($this->getTable('techies_insuranceplan/insuranceplan'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Insurance Plan ID'
    )
    ->addColumn(
        'name',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable'  => false,
        ),
        'Name'
    )
    ->addColumn(
        'website',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'website'
    )
    ->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(),
        'Enabled'
    )
    ->addColumn(
        'updated_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Insurance Plan Modification Time'
    )
    ->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Insurance Plan Creation Time'
    ) 
    ->setComment('Insurance Plan Table');
$this->getConnection()->createTable($table);
$this->endSetup();
