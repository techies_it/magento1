<?php
/**
 * Techies_InsurancePlan extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Techies
 * @package        Techies_InsurancePlan
 * @copyright      Copyright (c) 2016
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * InsurancePlan default helper
 *
 * @category    Techies
 * @package     Techies_InsurancePlan
 * @author      Bal Singh
 */
class Techies_InsurancePlan_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * convert array to options
     *
     * @access public
     * @param $options
     * @return array
     * @author Bal Singh
     */
    public function convertOptions($options)
    {
        $converted = array();
        foreach ($options as $option) {
            if (isset($option['value']) && !is_array($option['value']) &&
                isset($option['label']) && !is_array($option['label'])) {
                $converted[$option['value']] = $option['label'];
            }
        }
        return $converted;
    }
    public function avaiableInsurancePlan($term=null){
        $otherInsurance = Mage::getModel('techies_insuranceplan/insuranceplan')->getCollection()->addFieldToFilter('status',1)->addFieldToFilter('name', 'Other Insurance')->getFirstItem();
        $otherInsuranceArray = array();
        if($otherInsurance->getId()){
            $otherInsuranceArray[] = array('value'=>$otherInsurance->getId(),'label'=>$otherInsurance->getName());
        }
        $insurancePlanArray = array();
        $insurancePlanCollection = Mage::getModel('techies_insuranceplan/insuranceplan')->getCollection()
                ->addFieldToFilter('status',1)->addFieldToFilter('name',array('neq' => 'Other Insurance'));
        //$insurancePlanCollection->addFieldToFilter('name', array('like' => '%'.$term.'%'));
        $insurancePlanCollection->addFieldToFilter(
                array('name', 'search_tag'),
                    array(
                        array('like' => '%'.$term.'%'),
                        array('like' => '%'.$term.'%'),
                    )
                );
        $insurancePlanCollection->setOrder('name','ASC');
        foreach ($insurancePlanCollection as $insurancePlan){
            //$searchTag = $insurancePlan->getSearchTag();
            $id = $insurancePlan->getId();
            $insurancePlanName = $insurancePlan->getName();
            $insurancePlanArray[] = array('value'=>$id,'label'=>$insurancePlanName);
//            if($searchTag){
//                $searchTagArray = explode(',', $searchTag);
//                if(count($searchTagArray)){
//                    foreach ($searchTagArray as $data){
//                        $plan = trim($data);
//                        $plan = $plan." (Do you mean: ".$insurancePlanName.")";
//                        $insurancePlanArray[] = array('value'=>$id,'label'=>$plan);
//                    }
//                }
//            }
        }
        $insurancePlanArray = array_merge($otherInsuranceArray, $insurancePlanArray);
        return $insurancePlanArray;
    }
}
