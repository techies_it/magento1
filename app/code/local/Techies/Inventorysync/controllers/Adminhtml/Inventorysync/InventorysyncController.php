<?php
/**
 * Techies_Inventorysync extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Techies
 * @package        Techies_Inventorysync
 * @copyright      Copyright (c) 2016
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Inventorysync admin controller
 *
 * @category    Techies
 * @package     Techies_Inventorysync
 * @author      Bal Singh
 */
class Techies_Inventorysync_Adminhtml_Inventorysync_InventorysyncController extends Techies_Inventorysync_Controller_Adminhtml_Inventorysync
{
    /**
     * init the inventorysync
     *
     * @access protected
     * @return Techies_Inventorysync_Model_Inventorysync
     */
    protected function _initInventorysync()
    {
        $inventorysyncId  = (int) $this->getRequest()->getParam('id');
        $inventorysync    = Mage::getModel('techies_inventorysync/inventory');
        if ($inventorysyncId) {
            $inventorysync->load($inventorysyncId);
        }
        Mage::register('current_inventorysync', $inventorysync);
        return $inventorysync;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('techies_inventorysync')->__('Inventorysync'))
             ->_title(Mage::helper('techies_inventorysync')->__('Inventorysyncs'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function exportCsvAction()
    {
        $fileName   = 'inventorysync.csv';
        $content    = $this->getLayout()->createBlock('techies_inventorysync/adminhtml_inventorysync_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function exportExcelAction()
    {
        $fileName   = 'inventorysync.xls';
        $content    = $this->getLayout()->createBlock('techies_inventorysync/adminhtml_inventorysync_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function exportXmlAction()
    {
        $fileName   = 'inventorysync.xml';
        $content    = $this->getLayout()->createBlock('techies_inventorysync/adminhtml_inventorysync_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Bal Singh
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('techies_inventorysync/inventorysync');
    }
}
