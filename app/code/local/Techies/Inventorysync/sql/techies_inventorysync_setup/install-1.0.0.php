<?php
$this->startSetup();
$table = $this->getConnection()
    ->newTable($this->getTable('techies_inventorysync/inventory'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'ID'
    )
    ->addColumn(
        'product_id',
        Varien_Db_Ddl_Table::TYPE_TEXT, 20,
        array(),
        'product_id'
    )
    ->addColumn(
        'sku',
        Varien_Db_Ddl_Table::TYPE_TEXT, 50,
        array(),
        'sku'
    )
    ->addColumn(
        'qty',
        Varien_Db_Ddl_Table::TYPE_TEXT, 10,
        array(),
        'qty'
    )
    ->addColumn(
        'qty_change',
        Varien_Db_Ddl_Table::TYPE_TEXT, 10,
        array(),
        'qty_change'
    )
    ->addColumn(
        'ardo_status',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(),
        '1= sync, 0= not sync, 2= error'
    )
    ->addColumn(
        'is_in_stock',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(),
        '1= in stock, 0= out of stock'
    )
    ->addColumn(
        'amedadirect_status',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(),
        '1= sync, 0= not sync, 2= error'
    )
    ->addColumn(
        'comitstores_status',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(),
        '1= sync, 0= not sync, 2= error'
    )->addColumn(
        'updated_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Inventory Modification Time'
    )
    ->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Inventory Creation Time'
    ) 
        
    ->setComment('Inventorysync Table');
$this->getConnection()->createTable($table);
$this->endSetup();

?>