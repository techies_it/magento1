<?php
$this->startSetup();
$table = $this->getConnection()
    ->newTable($this->getTable('techies_inventorysync/inventorystatus'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'ID'
    )
    ->addColumn(
        'inventorysync_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(),
        'inventorysync_id'
    )
    ->addColumn(
        'store',
        Varien_Db_Ddl_Table::TYPE_TEXT, 20,
        array(),
        'store'
    )
   ->addColumn(
        'faultcode',
        Varien_Db_Ddl_Table::TYPE_TEXT, 50,
        array(),
        'faultcode'
    ) ->addColumn(
        'faultstring',
        Varien_Db_Ddl_Table::TYPE_TEXT, null,
        array(),
        'faultstring'
    )
        
    ->setComment('Inventorysync Status Table');
$this->getConnection()->createTable($table);
$this->endSetup();

?>