<?php
/**
 * Techies_Inventorysync extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Techies
 * @package        Techies_Inventorysync
 * @copyright      Copyright (c) 2016
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Inventorysync admin block
 *
 * @category    Techies
 * @package     Techies_Inventorysync
 * @author      Bal Singh
 */
class Techies_Inventorysync_Block_Adminhtml_Inventorysync extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function __construct()
    {
        $this->_controller         = 'adminhtml_inventorysync';
        $this->_blockGroup         = 'techies_inventorysync';
        parent::__construct();
        $this->_headerText         = Mage::helper('techies_inventorysync')->__('Inventorysync_log');
        //$this->_updateButton('add', 'label', Mage::helper('techies_inventorysync')->__('Add Inventorysync'));
        $this->_removeButton('add');
    }
}
