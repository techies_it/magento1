<?php
/**
 * Techies_Inventorysync extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Techies
 * @package        Techies_Inventorysync
 * @copyright      Copyright (c) 2016
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Inventorysync admin grid block
 *
 * @category    Techies
 * @package     Techies_Inventorysync
 * @author      Bal Singh
 */
class Techies_Inventorysync_Block_Adminhtml_Inventorysync_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Bal Singh
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('inventorysyncGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return Techies_Inventorysync_Block_Adminhtml_Inventorysync_Grid
     * @author Bal Singh
     */
    protected function _prepareCollection()
    {
//        $collection = Mage::getModel('techies_inventorysync/inventory')
//            ->getCollection();
//        $collection->getSelect()->join( array('inventorysync_status'=> $collection->getTable('techies_inventorysync/inventorystatus')), 'inventorysync_status.inventorysync_id = main_table.entity_id', array('GROUP_CONCAT(inventorysync_status.store) store', 'inventorysync_status.faultcode', 'inventorysync_status.faultstring'));
//        $collection->getSelect()->group('inventorysync_status.inventorysync_id');
//               // echo $collection->getSelect();
//        $this->setCollection($collection);
//        return parent::_prepareCollection();
        $collection = Mage::getModel('techies_inventorysync/inventory')
            ->getCollection();
        $collection->getSelect()->joinLeft( array('inventorysync_status'=> $collection->getTable('techies_inventorysync/inventorystatus')), 'inventorysync_status.inventorysync_id = main_table.entity_id and inventorysync_status.store="ardo_status"', array('inventorysync_status.faultstring as ardo_error_message'));
        $collection->getSelect()->joinLeft( array('inventorysync_status2'=> $collection->getTable('techies_inventorysync/inventorystatus')), 'inventorysync_status2.inventorysync_id = main_table.entity_id and inventorysync_status2.store="comitstores_status"', array('inventorysync_status2.faultstring as comitstores_error_message'));
        //uncomment following to show error log only
        //$collection->getSelect()->where('(`inventorysync_status`.`store` IS NOT NULL or `inventorysync_status2`.`store` IS NOT NULL)');
        $collection->getSelect()->group('main_table.entity_id');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return Techies_Inventorysync_Block_Adminhtml_Inventorysync_Grid
     * @author Bal Singh
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            array(
                'header' => Mage::helper('techies_inventorysync')->__('Id'),
                'index'  => 'entity_id',
                'type'   => 'number'
            )
        );
        $this->addColumn(
            'sku',
            array(
                'header'    => Mage::helper('techies_inventorysync')->__('Sku'),
                'align'     => 'left',
                'index'     => 'sku',
            )
        );
//        $this->addColumn(
//            'store',
//            array(
//                'header'    => Mage::helper('techies_inventorysync')->__('Stores'),
//                'align'     => 'left',
//                'index'     => 'store',
//            )
//        );
        $this->addColumn(
            'comitstores_error_message',
            array(
                'header'    => Mage::helper('techies_inventorysync')->__('comitstores_error_message'),
                'align'     => 'left',
                'index'     => 'comitstores_error_message',
                'filter_index'=>'inventorysync_status2.faultstring',
            )
        );
        $this->addColumn(
            'ardo_error_message',
            array(
                'header'    => Mage::helper('techies_inventorysync')->__('ardo_error_message'),
                'align'     => 'left',
                'index'     => 'ardo_error_message',
                'filter_index'=>'inventorysync_status.faultstring',
            )
        );
        $this->addColumn(
            'created_at',
            array(
                'header' => Mage::helper('techies_inventorysync')->__('Created at'),
                'index'  => 'created_at',
                'width'  => '120px',
                'type'   => 'datetime',
            )
        );
        $this->addColumn(
            'updated_at',
            array(
                'header'    => Mage::helper('techies_inventorysync')->__('Updated at'),
                'index'     => 'updated_at',
                'width'     => '120px',
                'type'      => 'datetime',
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('techies_inventorysync')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('techies_inventorysync')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('techies_inventorysync')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * get the row url
     *
     * @access public
     * @param Techies_Inventorysync_Model_Inventorysync
     * @return string
     * @author Bal Singh
     */
    public function getRowUrl($row)
    {
        return;
        //return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Bal Singh
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return Techies_Inventorysync_Block_Adminhtml_Inventorysync_Grid
     * @author Bal Singh
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }
}
