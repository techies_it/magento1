<?php

class Techies_Inventorysync_Model_Inventorystatus extends Mage_Core_Model_Abstract {
    
    public function _construct()
    {
        $this->_init('techies_inventorysync/inventorystatus');
    }
    /**
     * Load object data
     *
     * @param   integer $id
     * @return  Mage_Core_Model_Abstract
     */
    public function load($id, $field=null)
    {
        $resource =  $this->_getResource();
        if (!$resource) {
            throw new UnexpectedValueException('Resource instance is not available');
        }

        return parent::load($id, $field);
    }
//    protected function _afterSave()
//    {
//        return parent::_afterSave();
//    }
    /*     * ** Config Functions *** */

}