<?php

class Techies_Inventorysync_Model_Inventory extends Mage_Core_Model_Abstract {
    
    public function _construct()
    {
        $this->_init('techies_inventorysync/inventory');
    }
    /**
     * Load object data
     *
     * @param   integer $id
     * @return  Mage_Core_Model_Abstract
     */
    public function load($id, $field=null)
    {
        $resource =  $this->_getResource();
        if (!$resource) {
            throw new UnexpectedValueException('Resource instance is not available');
        }

        return parent::load($id, $field);
    }
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()) {
            $this->setCreatedAt($now);
        }
        $this->setUpdatedAt($now);
        return $this;
    }
//    protected function _afterSave()
//    {
//        return parent::_afterSave();
//    }
    /*     * ** Config Functions *** */

}