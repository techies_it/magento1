<?php

class Techies_Inventorysync_Model_Observer {

    const QTY_UPDATED = 1;
    const QTY_NOT_UPDATED = 2;
    const XML_PATH_EMAIL_RECIPIENT  = 'techies_inventorysync/inventory_alert/email';
    const XML_PATH_NOTIFICATION_HOURS  = 'techies_inventorysync/inventory_alert/hours';
    const XML_PATH_NOTIFICATION_QTY  = 'techies_inventorysync/inventory_alert/alert_qty';
    const XML_PATH_EMAIL_SENDER     = 'catalog/productalert/email_identity';
    const XML_PATH_EMAIL_TEMPLATE   = 'techies_inventorysync/email/techies_inventory_alert_template';
    
    public static function checkApiCall(){
        $apiRunning = Mage::getSingleton('api/server')->getAdapter() != null;
        if(!empty($apiRunning)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Inventory sync with other stores
     */
    public static function inventorySync() {
        // sync invetory with ardo
        try {
            $url = Mage::getStoreConfig('techies_inventorysync/ardo/api_soap_url');
            if (!empty($url)) {
                $client = new SoapClient($url);

                // If somestuff requires api authentification,
                // then get a session token
                $apiUser = Mage::getStoreConfig('techies_inventorysync/ardo/api_user');
                $apiKey = Mage::getStoreConfig('techies_inventorysync/ardo/api_key');
                $session = $client->login($apiUser, $apiKey);

                $inventoryCollection = Mage::getModel('techies_inventorysync/inventory')->getCollection()
                        ->addFieldToFilter('ardo_status', array('eq' => 0));
                self::postInvetoryData($client, $session, $inventoryCollection, 'ardo_status');
                $client->endSession($session);
            }
        } catch (Exception $ex) {
            Mage::log(" Error :" . $ex, null, 'techies_inventory_sync.log');
        }
        //End of sync invetory with ardo
        // sync invetory with comitstores
        try {
            $url = Mage::getStoreConfig('techies_inventorysync/comitstores/api_soap_url');
            if (!empty($url)) {
                $client = new SoapClient($url);

                // If somestuff requires api authentification,
                // then get a session token
                $apiUser = Mage::getStoreConfig('techies_inventorysync/comitstores/api_user');
                $apiKey = Mage::getStoreConfig('techies_inventorysync/comitstores/api_key');
                $session = $client->login($apiUser, $apiKey);

                $inventoryCollection = Mage::getModel('techies_inventorysync/inventory')->getCollection()
                        ->addFieldToFilter('comitstores_status', array('eq' => 0));
                self::postInvetoryData($client, $session, $inventoryCollection, 'comitstores_status');
                $client->endSession($session);
            }
        } catch (Exception $ex) {
            Mage::log(" Error :" . $ex, null, 'techies_inventory_sync.log');
        }
        //End of sync invetory with comitstores
    }

    public static function postInvetoryData($client, $session, $inventoryCollection, $store) {
        foreach ($inventoryCollection as $inventory) {
            $productSKU = $inventory->getSku();
            $stockItemData = array(
                'qty' => $inventory->getQty(),
                    //'is_in_stock ' => $inventory->getIsInStock(),
            );
            try {
                $result = $client->call(
                        $session, 'techies_product_stock.update', array(
                    $productSKU,
                    $stockItemData
                        )
                );
            } catch (SoapFault $ex) {
                $faultstring = $ex->faultstring;
                $faultcode = $ex->faultcode;
                $data['inventorysync_id'] = $inventory->getId();
                $data['store'] = $store;
                $data['faultstring'] = $faultstring;
                $data['faultcode'] = $faultcode;

                $inventory->setData($store, self::QTY_NOT_UPDATED);
                $inventory->save();
                // save status error
                $inventoryStatus = Mage::getModel('techies_inventorysync/inventorystatus');
                $inventoryStatus->setData($data);
                $inventoryStatus->save();
                //Mage::log($ex, null, 'techies_inventory_sync.log');
            }
            if (isset($result) && $result == 1) {
                $inventory->setData($store, self::QTY_UPDATED);
                $inventory->save();
            }
        }
        return;
    }

    /**
     * 
     * @param type $params
     * Save Data to invetory sync table
     */
    public static function saveInventoryToSync($params) {
        try {
            $inventory = Mage::getModel('techies_inventorysync/inventory');
            $params['amedadirect_status'] = 1;
            $params['ardo_status'] = 0;
            $params['comitstores_status'] = 0;
            $inventory->setData($params);
            $inventory->save();
        } catch (Exception $ex) {
            Mage::log('POSTED DATA=>' . print_r($params, 1) . " Error :" . $ex, null, 'techies_inventory_sync.log');
        }
    }

    public function catalogInventorySave(Varien_Event_Observer $observer) {
        try {
            if(self::checkApiCall()){
                return;
            }
            $event = $observer->getEvent();
            $_item = $event->getItem();

            if ((int) $_item->getData('qty') != (int) $_item->getOrigData('qty')) {

                $params = array();
                $params['product_id'] = $_item->getProductId();
                $sku = Mage::getModel('catalog/product')->load($params['product_id'])->getSku();
                $params['qty'] = $_item->getQty();
                $params['sku'] = $sku;
                $params['qty_change'] = $_item->getQty() - $_item->getOrigData('qty');
                $params['is_in_stock'] = $_item->getIsInStock();
                self::saveInventoryToSync($params);
            }
        } catch (Exception $ex) {
            Mage::log($ex, null, 'techies_inventory_sync_event.log');
        }
        
    }

    /**
     * 
     * @param Varien_Event_Observer $observer
     * Event to handle inventory on submit order
     */
    public function subtractQuoteInventory(Varien_Event_Observer $observer) {
        try {
            if(self::checkApiCall()){
                return;
            }
            $quote = $observer->getEvent()->getQuote();
            $paramsArray = array();
            foreach ($quote->getAllItems() as $item) {
                $params = array();
                $params['product_id'] = $item->getProductId();
                $params['sku'] = $item->getSku();
                $params['qty'] = $item->getProduct()->getStockItem()->getQty();
                $params['qty'] = $params['qty'] - $item->getTotalQty();
                $params['qty_change'] = ($item->getTotalQty() * -1);
                $params['is_in_stock'] = $item->getIsInStock();
                $paramsArray[$params['sku']] = $params;
            }
        } catch (Exception $ex) {
            Mage::log($ex, null, 'techies_inventory_sync_event.log');
        }
        if (count($paramsArray)) {
            foreach ($paramsArray as $params) {
                self::saveInventoryToSync($params);
            }
        }
    }

    public function revertQuoteInventory(Varien_Event_Observer $observer) {
        try {
            if(self::checkApiCall()){
                return;
            }
            $quote = $observer->getEvent()->getQuote();
            foreach ($quote->getAllItems() as $item) {
                $params = array();
                $params['product_id'] = $item->getProductId();
                $params['sku'] = $item->getSku();
                $params['qty'] = $item->getProduct()->getStockItem()->getQty();
                $params['qty_change'] = ($item->getTotalQty());
                $params['is_in_stock'] = $item->getIsInStock();
                self::saveInventoryToSync($params);
            }
        } catch (Exception $ex) {
            Mage::log($ex, null, 'techies_inventory_sync_event.log');
        }
    }

    public function cancelOrderItem(Varien_Event_Observer $observer) {
        try {
            if(self::checkApiCall()){
                return;
            }
            $item = $observer->getEvent()->getItem();
            $qty = $item->getQtyOrdered() - max($item->getQtyShipped(), $item->getQtyInvoiced()) - $item->getQtyCanceled();
            $params = array();
            $params['product_id'] = $item->getProductId();
            $params['sku'] = $item->getSku();
            $params['qty'] = $item->getProduct()->getStockItem()->getQty();
            $params['qty_change'] = $qty;
            $params['is_in_stock'] = $item->getIsInStock();
            self::saveInventoryToSync($params);
        } catch (Exception $ex) {
            Mage::log($ex, null, 'techies_inventory_sync_event.log');
        }
    }

    /**
     * 
     * @param Varien_Event_Observer $observer
     * Handel event of credit memo
     */
    public function refundOrderInventory(Varien_Event_Observer $observer) {
        try {
            if(self::checkApiCall()){
                return;
            }
            $creditmemo = $observer->getEvent()->getCreditmemo();
            foreach ($creditmemo->getAllItems() as $item) {
                $params = array();
                $params['product_id'] = $item->getProductId();
                $params['sku'] = $item->getSku();
                $_item = Mage::getModel('catalog/product')->load($item->getProductId())->getStockItem();
                $params['qty'] = $_item->getQty();
                $params['qty_change'] = ($item->getQty());
                $params['is_in_stock'] = $_item->getIsInStock();
                self::saveInventoryToSync($params);
            }
        } catch (Exception $ex) {
            Mage::log($ex, null, 'techies_inventory_sync_event.log');
        }
    }

    /**
     * Delete old data
     */
    public function deleteOldData() {
        try {
            $date = date('Y-m-d H:i:s', strtotime("-2 week"));
            $inventoryArray = array();
            $inventoryCollection = Mage::getModel('techies_inventorysync/inventory')->getCollection()
                    ->addFieldToFilter('ardo_status', array('neq' => 0))
                    ->addFieldToFilter('comitstores_status', array('neq' => 0))
                    ->addFieldToFilter('created_at', array('lt' => $date));
            foreach ($inventoryCollection as $inventory) {
                $inventoryArray[] = $inventory->getId();
                $inventory->delete();
            }
            // delete inventory status    
            $inventoryCollection = Mage::getModel('techies_inventorysync/inventorystatus')->getCollection()
                    ->addFieldToFilter('inventorysync_id', array('in' => $inventoryArray));
            foreach ($inventoryCollection as $inventory) {
                $inventory->delete();
            }
        } catch (Exception $ex) {
            Mage::log($ex, null, 'techies_inventory_sync_event.log');
        }
    }
    /**
     * Low stock alert email
     */
    public function lowStockAlert(){
        // send email alert for zero inventory
        $emailR = Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT);
        $hours = Mage::getStoreConfig(self::XML_PATH_NOTIFICATION_HOURS);
        $hours = !empty($hours)?$hours:25;
        $currentDate = Mage::app()->getLocale()->date();
        $date = $currentDate->subHour($hours);
        $dateformat = self::_dateToUtc($date);
        $date = $dateformat->toString('y-M-d H:m:s');
        if(!empty($emailR)){
            $minQty = Mage::getStoreConfig(self::XML_PATH_NOTIFICATION_QTY);
            $minQty = !empty($minQty)?$minQty:1;
            $inventoryCollection = Mage::getModel('techies_inventorysync/inventory')->getCollection()
                    ->addFieldToFilter('main_table.qty', array('lt' => (int)$minQty))
                    ->addFieldToFilter('main_table.created_at', array('gt' => $date));
            $inventoryCollection->getSelect()->joinLeft(array('m2' => 'techies_inventorysync_product'),
        '(main_table.sku = m2.sku and main_table.entity_id < m2.entity_id)',array());
        $inventoryCollection->getSelect()->where('m2.entity_id IS NULL');
       
            $zeroInventoryQueue = array();
            if($inventoryCollection->count()){
                foreach ($inventoryCollection as $inventory){
                    $zeroInventoryQueue[$inventory->getSku()] = $inventory->getSku();
                }
            }
            
            if(count($zeroInventoryQueue)){
                $dataArary['sku'] = implode('<br> ', $zeroInventoryQueue);
                $dataArary['qty'] = $minQty;
                $postObject = new Varien_Object();
                $postObject->setData($dataArary);
                 try {
                        //load the custom template to the email  
                        $mailTemplate = Mage::getModel('core/email_template');
                        /* @var $mailTemplate Mage_Core_Model_Email_Template */
                        $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                            ->setReplyTo(self::XML_PATH_EMAIL_SENDER)
                            ->sendTransactional(
                                'techies_inventory_alert_template',
                                Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
                                $emailR,
                                null,
                                array('data' => $postObject)
                            );

                        @$mailTemplate->getSentSuccess();

                    } catch (Exception $e) {
                        
                    }
            }
        }
    }
    public static function _dateToUtc($date)
    {
        if ($date === null) {
            return null;
        }
        $dateUtc = new Zend_Date($date);
        $dateUtc->setTimezone('Etc/UTC');
        return $dateUtc;
    }

}
