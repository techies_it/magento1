<?php
class Techies_Inventorysync_Model_Resource_Inventory_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        parent::_construct();
        $this->_init('techies_inventorysync/inventory');
    }
}
