<?php
class Techies_Inventorysync_Model_Resource_Inventorystatus extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('techies_inventorysync/inventorystatus', 'entity_id');
    }
}