<?php
/**
 * Techies_Mealoption extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Techies
 * @package        Techies_Mealoption
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Admin search model
 *
 * @category    Techies
 * @package     Techies_Mealoption
 * @author      Bal Singh
 */
class Techies_Mealoption_Model_Adminhtml_Search_Mealoption extends Varien_Object
{
    /**
     * Load search results
     *
     * @access public
     * @return Techies_Mealoption_Model_Adminhtml_Search_Mealoption
     * @author Bal Singh
     */
    public function load()
    {
        $arr = array();
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($arr);
            return $this;
        }
        $collection = Mage::getResourceModel('techies_mealoption/mealoption_collection')
            ->addFieldToFilter('title', array('like' => $this->getQuery().'%'))
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();
        foreach ($collection->getItems() as $mealoption) {
            $arr[] = array(
                'id'          => 'mealoption/1/'.$mealoption->getId(),
                'type'        => Mage::helper('techies_mealoption')->__('Mealoption'),
                'name'        => $mealoption->getTitle(),
                'description' => $mealoption->getTitle(),
                'url' => Mage::helper('adminhtml')->getUrl(
                    '*/mealoption_mealoption/edit',
                    array('id'=>$mealoption->getId())
                ),
            );
        }
        $this->setResults($arr);
        return $this;
    }
}
