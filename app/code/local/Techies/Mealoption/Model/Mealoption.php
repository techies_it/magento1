<?php
/**
 * Techies_Mealoption extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Techies
 * @package        Techies_Mealoption
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Mealoption model
 *
 * @category    Techies
 * @package     Techies_Mealoption
 * @author      Bal Singh
 */
class Techies_Mealoption_Model_Mealoption extends Mage_Core_Model_Abstract
{
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'techies_mealoption_mealoption';
    const CACHE_TAG = 'techies_mealoption_mealoption';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'techies_mealoption_mealoption';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'mealoption';

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('techies_mealoption/mealoption');
    }

    /**
     * before save mealoption
     *
     * @access protected
     * @return Techies_Mealoption_Model_Mealoption
     * @author Bal Singh
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()) {
            $this->setCreatedAt($now);
        }
        $this->setUpdatedAt($now);
        return $this;
    }

    /**
     * save mealoption relation
     *
     * @access public
     * @return Techies_Mealoption_Model_Mealoption
     * @author Bal Singh
     */
    protected function _afterSave()
    {
        return parent::_afterSave();
    }

    /**
     * get default values
     *
     * @access public
     * @return array
     * @author Bal Singh
     */
    public function getDefaultValues()
    {
        $values = array();
        $values['status'] = 1;
        return $values;
    }
    
}
