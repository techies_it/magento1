<?php

class Techies_Mealoption_Model_Observer {

    /**
     * 
     * @param Varien_Event_Observer $observer
     * This method is used on before save product event and re-associate carb and side options with current product
     */
    public function addCustomOptionToProduct(Varien_Event_Observer $observer) {
        if ($actionInstance = Mage::app()->getFrontController()->getAction()) {
            $action = $actionInstance->getFullActionName();
            if ($action == 'adminhtml_catalog_product_save') { //if on admin save action
                $product = $observer->getEvent()->getProduct();
                // Generate carbs and side option array
                if ($product->getEnableSideOption()) {
                    $optionData['side'] = self::getSideOption();
                }
                if ($product->getEnableCarbOption()) {
                    $optionData['carb'] = self::getCarbOption();
                }

                $addCustomOption['side'] = true;
                $addCustomOption['carb'] = true;
                //check that we haven't made the option already
                $options = $product->getOptions();
                //$options = $product->getOptions();
                if ($options) {
                    foreach ($options as $option) {
                        if (strtolower($option['title']) == 'side' && $option['type'] == 'drop_down' && !$option['is_delete']) {
                            //we've already added the option
                            $addCustomOption['side'] = false;
                            unset($optionData['side']);
                        }
                        if (strtolower($option['title']) == 'carb' && $option['type'] == 'drop_down' && !$option['is_delete']) {
                            //we've already added the option
                            $addCustomOption['carb'] = false;
                            unset($optionData['carb']);
                        }
                    }
                }

                // add carb or side option to product
                //don't use it like this because it has no effect in catalog_product_save_before event
                //$product->setProductOptions($options);
                //use it this way. Populate `$product->getOptionInstance()` directly
                $optionInstance = $product->getOptionInstance();
                if (isset($optionData['carb']) || isset($optionData['side'])) {
                    $product->setCanSaveCustomOptions(true);
                    if (isset($addCustomOption['side']) && $addCustomOption['side'] == true && isset($optionData['side'])) {
                        $optionInstance->addOption($optionData['side']);
                    }
                    if (isset($addCustomOption['carb']) && $addCustomOption['carb'] == true && isset($optionData['carb'])) {
                        $optionInstance->addOption($optionData['carb']);
                    }
                    //don't forget to state that the product has custom options
                    $product->setHasOptions(true);
                }
                // End of add carb or side option to product
            }
        }
    }

    /**
     * 
     * @return type
     * Generate carb option array
     */
    function getCarbOption() {
        $collection = Mage::helper('techies_mealoption')->getCarbCollection();
        $values = array();
        foreach ($collection as $option) {
            $values[] = array('title' => $option->getTitle(),
                            'price' => $option->getPrice(),
                            'price_type' => 'fixed',
                            'sort_order' => '1'
                            );
        }
        $carb = array(
            'title' => 'Carb',
            'type' => 'drop_down', // could be drop_down ,checkbox , multiple
            'is_require' => 0,
            'sort_order' => 0,
            'values' => $values
        );
        return $carb;
    }

    /**
     * 
     * @return type
     * Generate side option array
     */
    function getSideOption() {
        $collection = Mage::helper('techies_mealoption')->getSideCollection();
        $values = array();
        foreach ($collection as $option) {
            $values[] = array('title' => $option->getTitle(),
                            'price' => $option->getPrice(),
                            'price_type' => 'fixed',
                            'sort_order' => '1'
                            );
        }
        $side = array(
            'title' => 'Side',
            'type' => 'drop_down', // could be drop_down ,checkbox , multiple
            'is_require' => 0,
            'sort_order' => 0,
            'values' => $values
        );
        return $side;
    }

    /**
     * 
     * @param Varien_Event_Observer $observer
     * This method is used on after save Meal Options and re-associate carb and side options with products
     */
    public function addUpdateCustomOptionToRelatedProduct(Varien_Event_Observer $observer) {
        $mealoption = $observer->getEvent()->getMealoption();

        // Get the products which has enabled carb and side options
        $productCollection = Mage::getModel('catalog/product')->getCollection();
        $productCollection->addAttributeToFilter(
                array(
                    array('attribute' => 'enable_side_option', 'eq' => '1'),
                    array('attribute' => 'enable_carb_option', 'eq' => '1')
        ));

        foreach ($productCollection as $productData) {
            $product = Mage::getModel('catalog/product')->load($productData->getId());

            // Delete existing carb or side option from product
            $customOptions = $product->getOptions();
            foreach ($customOptions as $option) {
                if (strtolower($option['title']) == 'side' && $option['type'] == 'drop_down') {
                    //we've already added the option
                    $option->delete();
                }
                if (strtolower($option['title']) == 'carb' && $option['type'] == 'drop_down') {
                    //we've already added the option
                    $option->delete();
                }
            }
            // End Of Delete existing carb or side option from product
            //$product->setHasOptions(0);
            //$product->save();
            // Get carb or side option for product
            if ($product->getEnableSideOption()) {
                    $optionData['side'] = self::getSideOption();
                }
            if ($product->getEnableCarbOption()) {
                $optionData['carb'] = self::getCarbOption();
            }
            // add carb or side option to product
            if (isset($optionData['carb']) || isset($optionData['side'])) {
                $optionInstance = $product->getOptionInstance()->unsetOptions();
                $product->setHasOptions(1);
                if (isset($optionData['carb'])) {
                    $optionInstance->addOption($optionData['carb']);
                }
                if (isset($optionData['side'])) {
                    $optionInstance->addOption($optionData['side']);
                }
                $optionInstance->setProduct($product);
            }
            $product->save();
            unset($optionData);
            unset($product);
        }
    }

}
