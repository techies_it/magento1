<?php
/**
 * Techies_Mealoption extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Techies
 * @package        Techies_Mealoption
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Mealoption admin controller
 *
 * @category    Techies
 * @package     Techies_Mealoption
 * @author      Bal Singh
 */
class Techies_Mealoption_Adminhtml_Mealoption_MealoptionController extends Techies_Mealoption_Controller_Adminhtml_Mealoption
{
    /**
     * init the mealoption
     *
     * @access protected
     * @return Techies_Mealoption_Model_Mealoption
     */
    protected function _initMealoption()
    {
        $mealoptionId  = (int) $this->getRequest()->getParam('id');
        $mealoption    = Mage::getModel('techies_mealoption/mealoption');
        if ($mealoptionId) {
            $mealoption->load($mealoptionId);
        }
        Mage::register('current_mealoption', $mealoption);
        return $mealoption;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('techies_mealoption')->__('Meal Option'))
             ->_title(Mage::helper('techies_mealoption')->__('Mealoptions'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit mealoption - action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function editAction()
    {
        $mealoptionId    = $this->getRequest()->getParam('id');
        $mealoption      = $this->_initMealoption();
        if ($mealoptionId && !$mealoption->getId()) {
            $this->_getSession()->addError(
                Mage::helper('techies_mealoption')->__('This mealoption no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getMealoptionData(true);
        if (!empty($data)) {
            $mealoption->setData($data);
        }
        Mage::register('mealoption_data', $mealoption);
        $this->loadLayout();
        $this->_title(Mage::helper('techies_mealoption')->__('Meal Option'))
             ->_title(Mage::helper('techies_mealoption')->__('Mealoptions'));
        if ($mealoption->getId()) {
            $this->_title($mealoption->getTitle());
        } else {
            $this->_title(Mage::helper('techies_mealoption')->__('Add mealoption'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new mealoption action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save mealoption - action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('mealoption')) {
            try {
                $mealoption = $this->_initMealoption();
                $mealoption->addData($data);
                $mealoption->save();
                // add options to related products
                Mage::dispatchEvent('techies_mealoption_save_after', array('mealoption' => $mealoption));
                //
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('techies_mealoption')->__('Mealoption was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $mealoption->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setMealoptionData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('techies_mealoption')->__('There was a problem saving the mealoption.')
                );
                Mage::getSingleton('adminhtml/session')->setMealoptionData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('techies_mealoption')->__('Unable to find mealoption to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete mealoption - action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $mealoption = Mage::getModel('techies_mealoption/mealoption');
                $mealoption->setId($this->getRequest()->getParam('id'))->delete();
                Mage::dispatchEvent('techies_mealoption_delete_after', array('mealoption' => $mealoption));
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('techies_mealoption')->__('Mealoption was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('techies_mealoption')->__('There was an error deleting mealoption.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('techies_mealoption')->__('Could not find mealoption to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete mealoption - action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function massDeleteAction()
    {
        $mealoptionIds = $this->getRequest()->getParam('mealoption');
        if (!is_array($mealoptionIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('techies_mealoption')->__('Please select mealoptions to delete.')
            );
        } else {
            try {
                foreach ($mealoptionIds as $mealoptionId) {
                    $mealoption = Mage::getModel('techies_mealoption/mealoption');
                    $mealoption->setId($mealoptionId)->delete();
                }
                Mage::dispatchEvent('techies_mealoption_mass_actions_after', array('mealoption' => $mealoption));
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('techies_mealoption')->__('Total of %d mealoptions were successfully deleted.', count($mealoptionIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('techies_mealoption')->__('There was an error deleting mealoptions.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function massStatusAction()
    {
        $mealoptionIds = $this->getRequest()->getParam('mealoption');
        if (!is_array($mealoptionIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('techies_mealoption')->__('Please select mealoptions.')
            );
        } else {
            try {
                foreach ($mealoptionIds as $mealoptionId) {
                $mealoption = Mage::getSingleton('techies_mealoption/mealoption')->load($mealoptionId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                Mage::dispatchEvent('techies_mealoption_mass_actions_after', array('mealoption' => $mealoption));
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d mealoptions were successfully updated.', count($mealoptionIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('techies_mealoption')->__('There was an error updating mealoptions.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass Category change - action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function massOptionCategoryAction()
    {
        $mealoptionIds = $this->getRequest()->getParam('mealoption');
        if (!is_array($mealoptionIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('techies_mealoption')->__('Please select mealoptions.')
            );
        } else {
            try {
                foreach ($mealoptionIds as $mealoptionId) {
                $mealoption = Mage::getSingleton('techies_mealoption/mealoption')->load($mealoptionId)
                    ->setOptionCategory($this->getRequest()->getParam('flag_option_category'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                Mage::dispatchEvent('techies_mealoption_mass_actions_after', array('mealoption' => $mealoption));
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d mealoptions were successfully updated.', count($mealoptionIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('techies_mealoption')->__('There was an error updating mealoptions.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function exportCsvAction()
    {
        $fileName   = 'mealoption.csv';
        $content    = $this->getLayout()->createBlock('techies_mealoption/adminhtml_mealoption_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function exportExcelAction()
    {
        $fileName   = 'mealoption.xls';
        $content    = $this->getLayout()->createBlock('techies_mealoption/adminhtml_mealoption_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function exportXmlAction()
    {
        $fileName   = 'mealoption.xml';
        $content    = $this->getLayout()->createBlock('techies_mealoption/adminhtml_mealoption_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Bal Singh
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('techies_mealoption/mealoption');
    }
}
