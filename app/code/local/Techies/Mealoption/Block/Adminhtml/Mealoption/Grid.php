<?php
/**
 * Techies_Mealoption extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Techies
 * @package        Techies_Mealoption
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Mealoption admin grid block
 *
 * @category    Techies
 * @package     Techies_Mealoption
 * @author      Bal Singh
 */
class Techies_Mealoption_Block_Adminhtml_Mealoption_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Bal Singh
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('mealoptionGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return Techies_Mealoption_Block_Adminhtml_Mealoption_Grid
     * @author Bal Singh
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('techies_mealoption/mealoption')
            ->getCollection();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return Techies_Mealoption_Block_Adminhtml_Mealoption_Grid
     * @author Bal Singh
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            array(
                'header' => Mage::helper('techies_mealoption')->__('Id'),
                'index'  => 'entity_id',
                'type'   => 'number'
            )
        );
        $this->addColumn(
            'title',
            array(
                'header'    => Mage::helper('techies_mealoption')->__('Title'),
                'align'     => 'left',
                'index'     => 'title',
            )
        );
        
        $this->addColumn(
            'status',
            array(
                'header'  => Mage::helper('techies_mealoption')->__('Status'),
                'index'   => 'status',
                'type'    => 'options',
                'options' => array(
                    '1' => Mage::helper('techies_mealoption')->__('Enabled'),
                    '0' => Mage::helper('techies_mealoption')->__('Disabled'),
                )
            )
        );
        $this->addColumn(
            'option_category',
            array(
                'header' => Mage::helper('techies_mealoption')->__('Category'),
                'index'  => 'option_category',
                'type'  => 'options',
                'options' => Mage::helper('techies_mealoption')->convertOptions(
                    Mage::getModel('techies_mealoption/mealoption_attribute_source_optioncategory')->getAllOptions(false)
                )

            )
        );
        $this->addColumn(
            'price',
            array(
                'header' => Mage::helper('techies_mealoption')->__('Price'),
                'index'  => 'price',
                'type'=> 'number',

            )
        );
        if (!Mage::app()->isSingleStoreMode() && !$this->_isExport) {
            $this->addColumn(
                'store_id',
                array(
                    'header'     => Mage::helper('techies_mealoption')->__('Store Views'),
                    'index'      => 'store_id',
                    'type'       => 'store',
                    'store_all'  => true,
                    'store_view' => true,
                    'sortable'   => false,
                    'filter_condition_callback'=> array($this, '_filterStoreCondition'),
                )
            );
        }
        $this->addColumn(
            'created_at',
            array(
                'header' => Mage::helper('techies_mealoption')->__('Created at'),
                'index'  => 'created_at',
                'width'  => '120px',
                'type'   => 'datetime',
            )
        );
        $this->addColumn(
            'updated_at',
            array(
                'header'    => Mage::helper('techies_mealoption')->__('Updated at'),
                'index'     => 'updated_at',
                'width'     => '120px',
                'type'      => 'datetime',
            )
        );
        $this->addColumn(
            'action',
            array(
                'header'  =>  Mage::helper('techies_mealoption')->__('Action'),
                'width'   => '100',
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('techies_mealoption')->__('Edit'),
                        'url'     => array('base'=> '*/*/edit'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'is_system' => true,
                'sortable'  => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('techies_mealoption')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('techies_mealoption')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('techies_mealoption')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return Techies_Mealoption_Block_Adminhtml_Mealoption_Grid
     * @author Bal Singh
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('mealoption');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label'=> Mage::helper('techies_mealoption')->__('Delete'),
                'url'  => $this->getUrl('*/*/massDelete'),
                'confirm'  => Mage::helper('techies_mealoption')->__('Are you sure?')
            )
        );
        $this->getMassactionBlock()->addItem(
            'status',
            array(
                'label'      => Mage::helper('techies_mealoption')->__('Change status'),
                'url'        => $this->getUrl('*/*/massStatus', array('_current'=>true)),
                'additional' => array(
                    'status' => array(
                        'name'   => 'status',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('techies_mealoption')->__('Status'),
                        'values' => array(
                            '1' => Mage::helper('techies_mealoption')->__('Enabled'),
                            '0' => Mage::helper('techies_mealoption')->__('Disabled'),
                        )
                    )
                )
            )
        );
        $this->getMassactionBlock()->addItem(
            'option_category',
            array(
                'label'      => Mage::helper('techies_mealoption')->__('Change Category'),
                'url'        => $this->getUrl('*/*/massOptionCategory', array('_current'=>true)),
                'additional' => array(
                    'flag_option_category' => array(
                        'name'   => 'flag_option_category',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('techies_mealoption')->__('Category'),
                        'values' => Mage::getModel('techies_mealoption/mealoption_attribute_source_optioncategory')
                            ->getAllOptions(true),

                    )
                )
            )
        );
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param Techies_Mealoption_Model_Mealoption
     * @return string
     * @author Bal Singh
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Bal Singh
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return Techies_Mealoption_Block_Adminhtml_Mealoption_Grid
     * @author Bal Singh
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

    /**
     * filter store column
     *
     * @access protected
     * @param Techies_Mealoption_Model_Resource_Mealoption_Collection $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @return Techies_Mealoption_Block_Adminhtml_Mealoption_Grid
     * @author Bal Singh
     */
    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }
        $collection->addStoreFilter($value);
        return $this;
    }
}
