<?php
/**
 * Techies_Mealoption extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Techies
 * @package        Techies_Mealoption
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Mealoption admin edit form
 *
 * @category    Techies
 * @package     Techies_Mealoption
 * @author      Bal Singh
 */
class Techies_Mealoption_Block_Adminhtml_Mealoption_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'techies_mealoption';
        $this->_controller = 'adminhtml_mealoption';
        $this->_updateButton(
            'save',
            'label',
            Mage::helper('techies_mealoption')->__('Save Mealoption')
        );
        $this->_updateButton(
            'delete',
            'label',
            Mage::helper('techies_mealoption')->__('Delete Mealoption')
        );
        $this->_addButton(
            'saveandcontinue',
            array(
                'label'   => Mage::helper('techies_mealoption')->__('Save And Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class'   => 'save',
            ),
            -100
        );
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * get the edit form header
     *
     * @access public
     * @return string
     * @author Bal Singh
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_mealoption') && Mage::registry('current_mealoption')->getId()) {
            return Mage::helper('techies_mealoption')->__(
                "Edit Mealoption '%s'",
                $this->escapeHtml(Mage::registry('current_mealoption')->getTitle())
            );
        } else {
            return Mage::helper('techies_mealoption')->__('Add Mealoption');
        }
    }
}
