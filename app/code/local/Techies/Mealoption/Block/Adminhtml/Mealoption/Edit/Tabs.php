<?php
/**
 * Techies_Mealoption extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Techies
 * @package        Techies_Mealoption
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Mealoption admin edit tabs
 *
 * @category    Techies
 * @package     Techies_Mealoption
 * @author      Bal Singh
 */
class Techies_Mealoption_Block_Adminhtml_Mealoption_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Bal Singh
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('mealoption_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('techies_mealoption')->__('Mealoption'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return Techies_Mealoption_Block_Adminhtml_Mealoption_Edit_Tabs
     * @author Bal Singh
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_mealoption',
            array(
                'label'   => Mage::helper('techies_mealoption')->__('Mealoption'),
                'title'   => Mage::helper('techies_mealoption')->__('Mealoption'),
                'content' => $this->getLayout()->createBlock(
                    'techies_mealoption/adminhtml_mealoption_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addTab(
                'form_store_mealoption',
                array(
                    'label'   => Mage::helper('techies_mealoption')->__('Store views'),
                    'title'   => Mage::helper('techies_mealoption')->__('Store views'),
                    'content' => $this->getLayout()->createBlock(
                        'techies_mealoption/adminhtml_mealoption_edit_tab_stores'
                    )
                    ->toHtml(),
                )
            );
        }
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve mealoption entity
     *
     * @access public
     * @return Techies_Mealoption_Model_Mealoption
     * @author Bal Singh
     */
    public function getMealoption()
    {
        return Mage::registry('current_mealoption');
    }
}
