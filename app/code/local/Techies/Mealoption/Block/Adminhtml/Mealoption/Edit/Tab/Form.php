<?php
/**
 * Techies_Mealoption extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Techies
 * @package        Techies_Mealoption
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Mealoption edit form tab
 *
 * @category    Techies
 * @package     Techies_Mealoption
 * @author      Bal Singh
 */
class Techies_Mealoption_Block_Adminhtml_Mealoption_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return Techies_Mealoption_Block_Adminhtml_Mealoption_Edit_Tab_Form
     * @author Bal Singh
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('mealoption_');
        $form->setFieldNameSuffix('mealoption');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'mealoption_form',
            array('legend' => Mage::helper('techies_mealoption')->__('Mealoption'))
        );

        $fieldset->addField(
            'title',
            'text',
            array(
                'label' => Mage::helper('techies_mealoption')->__('Title'),
                'name'  => 'title',
                'required'  => true,
                'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'option_category',
            'select',
            array(
                'label' => Mage::helper('techies_mealoption')->__('Category'),
                'name'  => 'option_category',
                'required'  => true,
                'class' => 'required-entry',

                'values'=> Mage::getModel('techies_mealoption/mealoption_attribute_source_optioncategory')->getAllOptions(true),
           )
        );

        $fieldset->addField(
            'price',
            'text',
            array(
                'label' => Mage::helper('techies_mealoption')->__('Price'),
                'name'  => 'price',
                'required'  => true,
                'class' => 'required-entry',

           )
        );
        $fieldset->addField(
            'weight',
            'text',
            array(
                'label' => Mage::helper('techies_mealoption')->__('Weight'),
                'name'  => 'weight',
                'required'  => false,
               // 'class' => 'required-entry',

           )
        );
        $fieldset->addField(
            'status',
            'select',
            array(
                'label'  => Mage::helper('techies_mealoption')->__('Status'),
                'name'   => 'status',
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('techies_mealoption')->__('Enabled'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('techies_mealoption')->__('Disabled'),
                    ),
                ),
            )
        );
        if (Mage::app()->isSingleStoreMode()) {
            $fieldset->addField(
                'store_id',
                'hidden',
                array(
                    'name'      => 'stores[]',
                    'value'     => Mage::app()->getStore(true)->getId()
                )
            );
            Mage::registry('current_mealoption')->setStoreId(Mage::app()->getStore(true)->getId());
        }
        $formValues = Mage::registry('current_mealoption')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getMealoptionData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getMealoptionData());
            Mage::getSingleton('adminhtml/session')->setMealoptionData(null);
        } elseif (Mage::registry('current_mealoption')) {
            $formValues = array_merge($formValues, Mage::registry('current_mealoption')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
