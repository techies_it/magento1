<?php
/**
 * Techies_Mealoption extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Techies
 * @package        Techies_Mealoption
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Mealoption admin block
 *
 * @category    Techies
 * @package     Techies_Mealoption
 * @author      Bal Singh
 */
class Techies_Mealoption_Block_Adminhtml_Mealoption extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Bal Singh
     */
    public function __construct()
    {
        $this->_controller         = 'adminhtml_mealoption';
        $this->_blockGroup         = 'techies_mealoption';
        parent::__construct();
        $this->_headerText         = Mage::helper('techies_mealoption')->__('Mealoption');
        $this->_updateButton('add', 'label', Mage::helper('techies_mealoption')->__('Add Mealoption'));

    }
}
