<?php
/**
 * Techies_Mealoption extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Techies
 * @package        Techies_Mealoption
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Mealoption default helper
 *
 * @category    Techies
 * @package     Techies_Mealoption
 * @author      Bal Singh
 */
class Techies_Mealoption_Helper_Data extends Mage_Core_Helper_Abstract
{
    const SIDE = 1;
    const CARB = 2;
    /**
     * convert array to options
     *
     * @access public
     * @param $options
     * @return array
     * @author Bal Singh
     */
    public function convertOptions($options)
    {
        $converted = array();
        foreach ($options as $option) {
            if (isset($option['value']) && !is_array($option['value']) &&
                isset($option['label']) && !is_array($option['label'])) {
                $converted[$option['value']] = $option['label'];
            }
        }
        return $converted;
    }
    public function getSideOptions(){
        $options = $this->getSideCollection()->toOptionArray();
        return $options;
    }
    public function getCarbOptions(){
        $options = $this->getCarbCollection()->toOptionArray();
        return $options;
    }
    public function getSideCollection(){
        $options = Mage::getModel('techies_mealoption/mealoption')->getCollection()
                ->addFieldToFilter('status', 1)->addFieldToFilter('option_category', self::SIDE);
        return $options;
    }
    public function getCarbCollection(){
        $options = Mage::getModel('techies_mealoption/mealoption')->getCollection()
                ->addFieldToFilter('status', 1)->addFieldToFilter('option_category', self::CARB);
        return $options;
    }
}
