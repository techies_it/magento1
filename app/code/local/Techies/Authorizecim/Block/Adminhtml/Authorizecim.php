<?php
class Techies_Authorizecim_Block_Adminhtml_Authorizecim
    extends Mage_Adminhtml_Block_Template
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{    
    public function __construct() {
        $this->setTemplate('techies_authorizecim/chargecard.phtml');
        parent::__construct();
    }

    public function getTabLabel() {
        return $this->__('Charge Customer Card');
    }

    public function getTabTitle() {
        return $this->__('Charge Customer Card');
    }

    public function canShowTab() {
        return true;
    }

    public function isHidden() {
        return false;
    }
    public function getTransactionUrl() {
        return $this->getUrl('*/authorizecim_authorizecim/createTransaction');
    }
    public function getOrder(){
        return Mage::registry('current_order');
    }
    public function getAuthorizecimProfile(){
        $order = Mage::registry('current_order');
        $orderId = $order->getId();
        $authorizecim = Mage::getModel('techies_authorizecim/authorizecim')->load($orderId,'order_id');
        return $authorizecim;
    }
} 
?>