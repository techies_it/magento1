<?php

class Techies_Authorizecim_Model_Authorizecim extends Mage_Core_Model_Abstract {
    
    public function _construct()
    {
        $this->_init('techies_authorizecim/authorizecim');
    }
    /**
     * Load object data
     *
     * @param   integer $id
     * @return  Mage_Core_Model_Abstract
     */
    public function load($id, $field=null)
    {
        $resource =  $this->_getResource();
        if (!$resource) {
            throw new UnexpectedValueException('Resource instance is not available');
        }

        return parent::load($id, $field);
    }
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()) {
            $this->setCreatedAt($now);
        }
        return $this;
    }
}