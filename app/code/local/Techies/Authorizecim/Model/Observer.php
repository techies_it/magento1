<?php

include_once Mage::getBaseDir() . '/lib/authorize_sdk/vendor/autoload.php';

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
define("AUTHORIZENET_LOG_FILE", Mage::getBaseDir()."/var/log/authorizenetCIM.log");
class Techies_Authorizecim_Model_Observer {

    /**
     * 
     * @param type $event
     * This function is used to creat cusotmer Authorize.net CIM profile
     */
    public function saveOrderAfter($event) {
        try {
            $order = $event->getOrder();
            $orderId = $order->getId();
            $payment = $order->getPayment();
            $paymentCode = $payment->getMethodInstance()->getCode();
            if($paymentCode!='authorizenet'){
                return;
            }
            
            $transaction = Mage::getModel('sales/order_payment_transaction')->getCollection()->addAttributeToFilter('order_id', array('eq' => $orderId))->getFirstItem();
            $transactionId = $transaction->getTxnId();
            $customerId = $order->getCustomerId();
            $customerEmail = $order->getCustomerEmail();
            // Common setup for API credentials
            $apilogin = Mage::getModel('paygate/authorizenet')->getConfigData('login');
            $apiTransactionKey = Mage::getModel('paygate/authorizenet')->getConfigData('trans_key');
            $istestMode = Mage::getModel('paygate/authorizenet')->getConfigData('test');
            $gatewayUrl =Mage::getModel('paygate/authorizenet')->getConfigData('cgi_url');
            
            if ($istestMode || strpos($gatewayUrl, 'test') !== false) {
                $apiUrl = \net\authorize\api\constants\ANetEnvironment::SANDBOX;
            } else {
                $apiUrl = \net\authorize\api\constants\ANetEnvironment::PRODUCTION;
            }
            $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
            $merchantAuthentication->setName($apilogin);
            $merchantAuthentication->setTransactionKey($apiTransactionKey);

            $customerProfile = new AnetAPI\CustomerProfileBaseType();
            $customerProfile->setMerchantCustomerId($customerId);
            $customerProfile->setEmail($customerEmail);

            $request = new AnetAPI\CreateCustomerProfileFromTransactionRequest();
            $request->setMerchantAuthentication($merchantAuthentication);
            $request->setTransId($transactionId);
            // You can either specify the customer information in form of customerProfileBaseType object
            $request->setCustomer($customerProfile);

            $controller = new AnetController\CreateCustomerProfileFromTransactionController($request);
            $response = $controller->executeWithApiResponse($apiUrl);
            if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
                $customerProfileId = $response->getCustomerProfileId();
                $paymentProfileArray = $response->getCustomerPaymentProfileIdList();
                $paymentProfileId = $paymentProfileArray[0];
                $shippingAddressArray = $response->getCustomerShippingAddressIdList();
                $shippingAddressId = $shippingAddressArray[0];
                $cimProfileModel = Mage::getModel("techies_authorizecim/authorizecim")->getCollection()->addFieldToFilter('order_id', array('eq' => $orderId));
                if(!$cimProfileModel->getSize()){
                    $cimProfile = Mage::getModel("techies_authorizecim/authorizecim");
                    $cimProfile->setData(array('customer_email'=>$customerEmail,'customer_id'=>$customerId,'order_id'=>$orderId,'customer_profile_id'=>$customerProfileId,'payment_profile_id'=>$paymentProfileId,'shipping_address_id'=>$shippingAddressId));
                    $cimProfile->save();
                }
            } else {
                $responseMessage = "ERROR :  Invalid response\n";
                $errorMessages = $response->getMessages()->getMessage();
                $responseMessage .= "OrderId : ".$orderId."  Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "\n";
                Mage::log($responseMessage, null, 'authorizenetCIM.log');
            }
            
        } catch (Exception $e) {
            $responseMessage .= "OrderId : ".$orderId."  Error: Error in creating CIM profile";
            Mage::log($responseMessage, null, 'authorizenetCIM.log');
        }
        return;
    }

}
