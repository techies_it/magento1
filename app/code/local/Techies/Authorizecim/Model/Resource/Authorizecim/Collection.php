<?php
class Techies_Authorizecim_Model_Resource_Authorizecim_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        parent::_construct();
        $this->_init('techies_authorizecim/authorizecim');
    }
}
