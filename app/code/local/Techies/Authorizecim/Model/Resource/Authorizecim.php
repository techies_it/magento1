<?php
class Techies_Authorizecim_Model_Resource_Authorizecim extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('techies_authorizecim/authorizecim', 'entity_id');
    }
}