<?php

include_once Mage::getBaseDir() . '/lib/authorize_sdk/vendor/autoload.php';

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

define("AUTHORIZENET_LOG_FILE", Mage::getBaseDir() . "/var/log/authorizenetCIM.log");
/**
 * Techies_Authorizecim extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Techies
 * @package		Techies_Authorizecim
 * @copyright  	Copyright (c) 2016
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Authorizecim admin controller
 *
 * @category	Techies
 * @package		Techies_Authorizecim
 */
class Techies_Authorizecim_Adminhtml_Authorizecim_AuthorizecimController extends Mage_Adminhtml_Controller_Action {

    public function createTransactionAction() {
        try {
            $customerProfileId = $_POST['customer_profile_id'];
            $paymentProfileId = $_POST['payment_profile_id'];
            $transactionAmount = $_POST['transaction_amount'];
            $transactionAmount = number_format($transactionAmount, 2, '.', '');
            $orderId = $_POST['order_id'];
            $techiesAuthorizecimProfileId = $_POST['techies_authorizecim_profile_id'];
            $transactionComment = $_POST['transaction_comment'];
            // Common setup for API credentials
            $apilogin = Mage::getModel('paygate/authorizenet')->getConfigData('login');
            $apiTransactionKey = Mage::getModel('paygate/authorizenet')->getConfigData('trans_key');
            $istestMode = Mage::getModel('paygate/authorizenet')->getConfigData('test');
            $gatewayUrl = Mage::getModel('paygate/authorizenet')->getConfigData('cgi_url');

            if ($istestMode || strpos($gatewayUrl, 'test') !== false) {
                $apiUrl = \net\authorize\api\constants\ANetEnvironment::SANDBOX;
            } else {
                $apiUrl = \net\authorize\api\constants\ANetEnvironment::PRODUCTION;
            }
            $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
            $merchantAuthentication->setName($apilogin);
            $merchantAuthentication->setTransactionKey($apiTransactionKey);
            $refId = 'ref' . time();

            $profileToCharge = new AnetAPI\CustomerProfilePaymentType();
            $profileToCharge->setCustomerProfileId($customerProfileId);
            $paymentProfile = new AnetAPI\PaymentProfileType();
            $paymentProfile->setPaymentProfileId($paymentProfileId);
            $profileToCharge->setPaymentProfile($paymentProfile);
            $transactionRequestType = new AnetAPI\TransactionRequestType();
            $transactionRequestType->setTransactionType("authCaptureTransaction");
            $transactionRequestType->setAmount($transactionAmount);
            $transactionRequestType->setProfile($profileToCharge);

            $request = new AnetAPI\CreateTransactionRequest();
            $request->setMerchantAuthentication($merchantAuthentication);
            $request->setRefId($refId);
            $request->setTransactionRequest($transactionRequestType);
            $controller = new AnetController\CreateTransactionController($request);
            $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
            if ($response != null) {
                $tresponse = $response->getTransactionResponse();
                if (($tresponse != null) && ($tresponse->getResponseCode() == "1")) {
                    $transactionId = $tresponse->getTransId();
                    $authCode = $tresponse->getAuthCode();
                    // add comment to magento order
                    $shortPaymentProfileId = substr($paymentProfileId, -4);
                    $shortPaymentProfileId = str_pad($shortPaymentProfileId, 10, "*", STR_PAD_LEFT);
                    $comment = "Payment Profile Id: " . $shortPaymentProfileId . " Amount $" . $transactionAmount . " authorize and capture - successful. Authorize.Net Transaction ID " . $transactionId . ". " . $transactionComment;
                    $order = Mage::getModel('sales/order')->load($orderId);
                    $order->addStatusHistoryComment($comment);
                    $order->save();

                    $write = Mage::getSingleton("core/resource")->getConnection("core_write");
                    $query = "insert into techies_authorizecim_transaction "
                            . "(techies_authorizecim_profile_id, order_id, transaction_id, auth_code) values "
                            . "(:techies_authorizecim_profile_id, :order_id, :transaction_id, :auth_code)";
                    $binds = array('techies_authorizecim_profile_id' => $techiesAuthorizecimProfileId, 'order_id' => $orderId, 'transaction_id' => $transactionId, 'auth_code' => $authCode);
                    $write->query($query, $binds);
                    $message = "Transaction done successfully. Tranaction ID : " . $transactionId;
                } elseif (($tresponse != null) && ($tresponse->getResponseCode() == "2")) {
                    $message = "ERROR";
                } elseif (($tresponse != null) && ($tresponse->getResponseCode() == "4")) {
                    $message = "ERROR: HELD FOR REVIEW:";
                }else{
                    $errorMessages = $response->getMessages()->getMessage();
                    $message = "ERROR : ".$errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText();
                
                }
            } else {
                $message = "no response returned";
            }
        } catch (Exception $e) {
            $message .= "OrderId : " . $orderId . "  Error: Error in transaction";
            Mage::log($message, null, 'authorizenetCIM.log');
        }
        $json = json_encode(array($message));
        $this->getResponse()
                ->clearHeaders()
                ->setHeader('Content-Type', 'application/json')
                ->setBody($json);
    }

}
