<?php
$this->startSetup();
$table = $this->getConnection()
    ->newTable('techies_authorizecim_transaction')
    ->addColumn(
        'techies_authorizecim_profile_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'unsigned'  => true,
        ),
        'techies authorizecim profile id'
    )
    ->addColumn(
        'order_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'unsigned'  => true,
        ),
        'Order id'
    )
    ->addColumn(
        'transaction_id',
        Varien_Db_Ddl_Table::TYPE_TEXT, 20,
        array(),
        'transaction id'
    )
    ->addColumn(
        'auth_code',
         Varien_Db_Ddl_Table::TYPE_TEXT, 20,
        array(),
        'AUTH CODE'
    )
    ->setComment('authorizecim transaction code');
$this->getConnection()->createTable($table);
$this->endSetup();

?>