<?php
$this->startSetup();
$table = $this->getConnection()
    ->newTable($this->getTable('techies_authorizecim/authorizecim'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'ID'
    )
    ->addColumn(
        'customer_email',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'customer email'
    )
    ->addColumn(
        'customer_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(),
        'customer ID'
    )
    ->addColumn(
        'order_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(),
        'Order ID'
    )
    ->addColumn(
        'customer_profile_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(),
        'authorizecim customer profile ID'
    )
    ->addColumn(
        'payment_profile_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(),
        'authorizecim payment profile ID'
    )
    ->addColumn(
        'shipping_address_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(),
        'authorizecim shipping address ID'
    )->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Creation Time'
    )->setComment('Authorizecim Profile Table');
$this->getConnection()->createTable($table);
$this->endSetup();

?>