<?php
class Techies_Rewriteseositemap_Model_CategoryList{
    public function toOptionArray() {
        $collection = Mage::getModel('catalog/category')->getCollection()->addAttributeToSelect('name')
                ->addAttributeToFilter('name',array('notnull' => true))
                ->addAttributeToFilter('level', 2)->setOrder('name');
        $categoryArray = array();
        foreach ($collection as $_item){
            //echo "<pre>"; print_r($_item); exit;
            $categoryArray[] = array('value'=>$_item->getId(),'label'=>$_item->getName());
        }
        return $categoryArray;
    }
}