<?php
/**
 * Techies_Rewriteseositemap extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Techies
 * @package        Techies_Rewriteseositemap
 * @copyright      Copyright (c) 2016
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * ArdoReferral default helper
 *
 * @category    Techies
 * @package     Techies_Rewriteseositemap
 * @author      Bal Singh
 */
class Techies_Rewriteseositemap_Helper_Data extends Mage_Core_Helper_Abstract
{
}
