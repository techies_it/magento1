<?php
class Techies_Rewriteseositemap_Block_Sitemap_Tree_Category extends Mage_Catalog_Block_Seo_Sitemap_Tree_Category{
    protected function _prepareLayout()
    {
        $collection = parent::_prepareLayout();
        $categories = Mage::getStoreConfig('techies_rewriteseositemap/category/hidden_category');
        $categories = explode(',', $categories);
        if(count($categories)){
            foreach ($categories as $categoryId){
                $collection->getCollection()->addAttributeToFilter('path', array("nlike" => '%/'.$categoryId));
                $collection->getCollection()->addAttributeToFilter('path', array("nlike" => '%/'.$categoryId.'/%'));
            }
        }
        return $collection;
    }
}