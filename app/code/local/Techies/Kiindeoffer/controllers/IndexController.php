<?php

class Techies_Kiindeoffer_IndexController extends Mage_Core_Controller_Front_Action {
    const XML_PATH_EMAIL_RECIPIENT  = 'kiindeoffer/email/recipient_email';
    const XML_PATH_EMAIL_SENDER     = 'kiindeoffer/email/sender_email_identity';
    const XML_PATH_EMAIL_TEMPLATE   = 'kiindeoffer/email/email_template';
    public function indexAction() {


        $this->loadLayout();  //This function read all layout files and loads them in memory
        $post = $this->getRequest()->getPost();
        
        if ($post) {

            $refererUrl = '/kiinde-offer';
            Mage::app()->getFrontController()->getResponse()
                    ->setRedirect($refererUrl)
                    ->sendResponse();
        }
        $this->renderLayout(); //This function processes and displays all layout phtml and php files.
    }
    public function postAction()
    {
        $refererUrl = '/kiinde-offer';
            
        $post = $this->getRequest()->getPost();
        //echo "<pre>";        print_r($post); exit;
        if ( $post  && !empty($post['g-recaptcha-response'])) {
            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);

                $error = false;

                if (!Zend_Validate::is(trim($post['first_name']) , 'NotEmpty')) {
                    $error = true;
                }
                if (!Zend_Validate::is(trim($post['last_name']) , 'NotEmpty')) {
                    $error = true;
                }
                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }
                
                if (!Zend_Validate::is(trim($post['ardo_pump_serial_number']) , 'NotEmpty')) {
                    $error = true;
                }
                if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
                    $error = true;
                }
                if ($error) {
                    throw new Exception();
                }
                $mailTemplate = Mage::getModel('core/email_template');
                /* @var $mailTemplate Mage_Core_Model_Email_Template */
                $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                    ->setReplyTo($post['email'])
                    ->sendTransactional(
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE),
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT),
                        null,
                        array('data' => $postObject)
                    );

                if (!$mailTemplate->getSentSuccess()) {
                    throw new Exception();
                }

                $translate->setTranslateInline(true);

              //  Mage::getSingleton('customer/session')->addSuccess(Mage::helper('kiindeoffer')->__('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.'));
                Mage::app()->getFrontController()->getResponse()
                    ->setRedirect('/thank-you')
                    ->sendResponse();

                return;
            } catch (Exception $e) {
                $translate->setTranslateInline(true);

                Mage::getSingleton('customer/session')->addError(Mage::helper('kiindeoffer')->__('Unable to submit your request. Please, try again later'));
                Mage::app()->getFrontController()->getResponse()
                    ->setRedirect($refererUrl)
                    ->sendResponse();
                return;
            }

        } else {
            Mage::getSingleton('core/session')->setFormData($post);
            if($post && empty( $post['g-recaptcha-response'])){
                Mage::getSingleton('customer/session')->addError(Mage::helper('kiindeoffer')->__('Please Enter reCAPTCHA'));
            }
            Mage::app()->getFrontController()->getResponse()
                    ->setRedirect($refererUrl)
                    ->sendResponse();
        }
    }
}

