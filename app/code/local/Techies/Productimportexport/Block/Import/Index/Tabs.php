<?php
class Techies_Productimportexport_Block_Adminhtml_Import_Index_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    
    public function __construct()
    {
        parent::__construct();
        $this->setId('productimportexport_tabs');
        $this->setDestElementId('import_form');
        $this->setTitle(Mage::helper('techies_productimportexport')->__('Import Product'));
    }
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_productimportexport',
            array(
                'label'   => Mage::helper('bm_shopbybrand')->__('Import Product'),
                'title'   => Mage::helper('bm_shopbybrand')->__('Import Product'),
                
            )
        );
//        $this->addTab(
//            'form_meta_shopbybrand',
//            array(
//                'label'   => Mage::helper('bm_shopbybrand')->__('Meta'),
//                'title'   => Mage::helper('bm_shopbybrand')->__('Meta'),
//                'content' => $this->getLayout()->createBlock(
//                    'bm_shopbybrand/adminhtml_shopbybrand_edit_tab_meta'
//                )
//                ->toHtml(),
//            )
//        );
        
        
        return parent::_beforeToHtml();
    }
}
