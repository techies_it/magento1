<?php
$this->startSetup();
$table = $this->getConnection()
    ->newTable($this->getTable('techies_productimportexport/importproduct'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'ID'
    )
    ->addColumn(
        'file',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'file'
    )
    ->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(),
        '1= pending, 2= imported'
    )->addColumn(
        'updated_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Import Product Modification Time'
    )
    ->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Import Product Creation Time'
    ) 
        
    ->setComment('Import Product Table');
$this->getConnection()->createTable($table);
$this->endSetup();

?>