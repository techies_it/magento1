<?php

class Techies_Productimportexport_Model_Observer {
    const XML_PATH_EMAIL_RECIPIENT  = 'techies_productimportexport/email/recipient_email';
    const XML_PATH_EMAIL_SENDER     = 'techies_productimportexport/email/sender_email_identity';
    const XML_PATH_EMAIL_TEMPLATE   = 'techies_productimportexport/email/email_template';
    public function importDataFromCSV() {
        $importproductCollection = Mage::getModel('techies_productimportexport/importproduct')->getCollection()
                ->addFieldToFilter('status', array('eq' => 1));
        foreach ($importproductCollection as $importproduct) {
            $fileName = $importproduct->getFile();
            if (!file_exists($fileName)) {
                continue;
            }
            // get data from csv
            $csv = new Varien_File_Csv();
            $productArray = $csv->getData($fileName);
            $productId = '';
            $all_group_id = Mage_Customer_Model_Group::CUST_GROUP_ALL;
            foreach ($productArray as $key => $productData) {
                if ($productId == $productData[0]) {
                    continue;
                }
                $productId = $productData[0];
                $tierCount = (isset($productData[9]) && !empty($productData[9])) ? $productData[9] : 0;
                $product = Mage::getModel('catalog/product')->load($productId);
                if (!$product->getId()) {
                    continue;
                }
                if (isset($productData[3]) && !empty($productData[3])) {
                    $currentPrice = $product->getPrice();
                    if ($currentPrice != $productData[3]) {
                        $product->setPrice($productData[3]);
                    }
                }
                // calculate tier price
                $tierPrices = array();
                if ($tierCount > 0) {
                    $i = 1;
                    while ($i <= $tierCount) {
                        $nextKey = $key + $i;
                        if($productArray[$nextKey][0]==$productId){
                            $website_id = $productArray[$nextKey][6];
                            $all_groups = $productArray[$nextKey][7];
                            if($all_groups==1){
                                $customerGroup = $all_group_id;
                            }else{
                                $customerGroup = $productArray[$nextKey][8];
                            }
                            
                            $qty = $productArray[$nextKey][4];
                            $tierPrices[] = array(
                                'website_id' => $website_id,
                                // 'all_groups' => $all_groups,
                                'cust_group' => $customerGroup,
                                'price' => $productArray[$nextKey][5],
                                'price_qty' => $qty);
                        }
                        $i++;
                    }
                    $product->tier_price = $tierPrices;
                    //echo "<pre>";print_r($tierPrices);
                    //$product->setTierPrice($tierPrices);
                }
                $product->save();
            }
            $importproduct->setStatus(2);
            $importproduct->save();
            // reindex price
            $process = Mage::getModel('index/indexer')->getProcessByCode('catalog_product_price');
            $process->reindexAll();

            $mailTemplate = Mage::getModel('core/email_template');
            /* @var $mailTemplate Mage_Core_Model_Email_Template */
            $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                ->setReplyTo(self::XML_PATH_EMAIL_SENDER)
                ->sendTransactional(
                    Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE),
                    Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
                    Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT),
                    null,
                    array('data' => $postObject)
                );

            @$mailTemplate->getSentSuccess();
        }
        // check for pending files
        $importproductCollection1 = Mage::getModel('techies_productimportexport/importproduct')->getCollection()
                ->addFieldToFilter('status', array('eq' => 1))->count();
        if($importproductCollection1){
            $importproduct = Mage::getModel('techies_productimportexport/importproduct');
            $importproduct->setImportCronSchedule();
        }
    }

}
