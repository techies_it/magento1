<?php

class Techies_Productimportexport_Model_Importproduct extends Mage_Core_Model_Abstract {
    
    public function _construct()
    {
        $this->_init('techies_productimportexport/importproduct');
    }
    /**
     * Load object data
     *
     * @param   integer $id
     * @return  Mage_Core_Model_Abstract
     */
    public function load($id, $field=null)
    {
        $resource =  $this->_getResource();
        if (!$resource) {
            throw new UnexpectedValueException('Resource instance is not available');
        }

        return parent::load($id, $field);
    }
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()) {
            $this->setCreatedAt($now);
        }
        $this->setUpdatedAt($now);
        return $this;
    }
//    protected function _afterSave()
//    {
//        return parent::_afterSave();
//    }
    /*     * ** Config Functions *** */

    public function getCSVDirectory() {
        if (!file_exists(Mage::getBaseDir('base') . DIRECTORY_SEPARATOR . Mage::getStoreConfig('techies_productimportexport_options/configuration/productimportDir', Mage::app()->getStore())))
            mkdir(Mage::getBaseDir('base') . DIRECTORY_SEPARATOR . Mage::getStoreConfig('techies_productimportexport_options/configuration/productimportDir', Mage::app()->getStore()));
        return Mage::getBaseDir('base') . DIRECTORY_SEPARATOR . Mage::getStoreConfig('techies_productimportexport_options/configuration/productimportDir', Mage::app()->getStore());
    }
    /**
     * 
     * @param type $jobCode
     * @return boolean
     */
    public function setImportCronSchedule($jobCode = 'techies_productimportexport_importproduct'){
        $timecreated = strftime("%Y-%m-%d %H:%M:%S", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
        $timescheduled = strftime("%Y-%m-%d %H:%M:%S", mktime(date("H"), date("i") + 5, 00, date("m"), date("d"), date("Y")));
        $timescheduled2 = strftime("%Y-%m-%d %H:%M:%S", mktime(date("H"), date("i") + 15, 00, date("m"), date("d"), date("Y")));
        // set two cron schedule
        $schedule = Mage::getModel('cron/schedule');
        $schedule->setJobCode($jobCode)
                ->setCreatedAt($timecreated)
                ->setScheduledAt($timescheduled)
                ->setStatus(Mage_Cron_Model_Schedule::STATUS_PENDING)
                ->save();
        $schedule2 = Mage::getModel('cron/schedule');
        $schedule2->setJobCode($jobCode)
                ->setCreatedAt($timecreated)
                ->setScheduledAt($timescheduled2)
                ->setStatus(Mage_Cron_Model_Schedule::STATUS_PENDING)
                ->save();
        return true;
    }
}