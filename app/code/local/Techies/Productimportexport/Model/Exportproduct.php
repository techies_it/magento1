<?php

class Techies_Productimportexport_Model_Exportproduct extends Mage_Core_Model_Abstract {

    protected $products = array();
    protected $filename;
    protected $csv;
    protected $storeId = 1;
    public function export($post) {
        $this->getProducts($post);
        $this->prepareCSV();
        $this->saveCSV();
        $this->forceDownload();
        return;
    }

    protected function getProducts($post) {
        
        $products = Mage::getModel('catalog/product')->getCollection();
        if (isset($post['store']) && !empty($post['store']) && $post['store']!='') {
            $store = Mage::app()->getStore($post['store']);
            $products->addStoreFilter($store);
        }
        if (isset($post['sku']) && !empty($post['sku']) && $post['sku']!='') {
            $products->addAttributeToFilter('sku', array('like' => $post['sku'].'%'));
        }
        if (isset($post['status']) && !empty($post['status']) && $post['status']!='') {
            $products->addAttributeToFilter('status', array(
                    'eq' => $post['status']
                ));
        }
        $products->addAttributeToSelect('*');
        $products->addAttributeToSort('name', 'ASC')->load();
//        echo $products->getSelect();
//        echo count($products); exit;
        $this->products = $products;
        return;
    }

    protected function setHeaders() {
        $this->csv = '"Product ID","Name","SKU","Price","Tier QTY","Tier Price","Website ID","All Group","Customer Group","Tier Price Count"';
        return;
    }

    protected function prepareCSV() {
        $this->setHeaders();
        $count = 0;
        //echo "<pre>";        print_r($this->products); exit;
        foreach ($this->products as $product) {
            //$product = Mage::getModel('catalog/product')->load($productId);
            $sku = $product->getSku();
            $productName = str_replace('"', '', strip_tags($product->getName()));
            $price = round($product->getPrice(),2);
            $this->csv.=chr(10);
            //'"ID","NAME","SKU","PRICE","TIER QTY","TIER PRICE"'
            $this->csv.='"' . $product->getId() . '","'. $productName .'","' . $sku . '","' . $price . '"';
            $existingTierPrice = $product->getTierPrice();
            //$existingTierPrice = $product->getData('tier_price');
            // get tier prices
            if ($existingTierPrice){
                $product = Mage::getModel('catalog/product')->load($product->getId());
                $existingTierPrice = $product->getData('tier_price');
                $this->csv.=',"","","","","","'.  count($existingTierPrice).'"';
                //echo "<br>".$product->getId();
                foreach($existingTierPrice as $key=>$value){
                    $this->csv.=chr(10);
                    // get tier qty and price
                    $tierPrice = round($value['price'],2);
                    $tierQty = round($value['price_qty'],1);
                    $websiteId = $value['website_id'];
                    $all_groups = $value['all_groups'];
                    $cust_group = ($all_groups==1 || $value['cust_group']==32000)?0:$value['cust_group'];
                    $this->csv.='"' . $product->getId() . '","","' . $sku . '","","' . $tierQty . '","' . $tierPrice . '","' . $websiteId . '","' . $all_groups . '","' . $cust_group . '"';
                }
            }
            $count++;
        } 
        return;
    }

    protected function saveCSV() {
        $this->filename = $filename = 'productExport_' . date('jnY-His') . '.csv';
        file_put_contents($this->getCSVDirectory() . $filename, $this->csv);
        return;
    }

    protected function forceDownload() {
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename=' . $this->filename);
        header('Pragma: no-cache');
        readfile($this->getCSVDirectory() . $this->filename);
        exit;
        //return;
    }

    /*     * ** Config Functions *** */

    protected function getCSVDirectory() {
        if (!file_exists(Mage::getBaseDir('base') . DIRECTORY_SEPARATOR . Mage::getStoreConfig('techies_productimportexport_options/configuration/productimportexportDir', Mage::app()->getStore())))
            mkdir(Mage::getBaseDir('base') . DIRECTORY_SEPARATOR . Mage::getStoreConfig('techies_productimportexport_options/configuration/productimportexportDir', Mage::app()->getStore()));
        return Mage::getBaseDir('base') . DIRECTORY_SEPARATOR . Mage::getStoreConfig('techies_productimportexport_options/configuration/productimportexportDir', Mage::app()->getStore());
    }
}