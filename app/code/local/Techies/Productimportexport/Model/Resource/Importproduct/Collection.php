<?php
class Techies_Productimportexport_Model_Resource_Importproduct_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        parent::_construct();
        $this->_init('techies_productimportexport/importproduct');
    }
}
