<?php
class Techies_Productimportexport_Model_Resource_Importproduct extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('techies_productimportexport/importproduct', 'entity_id');
    }
}