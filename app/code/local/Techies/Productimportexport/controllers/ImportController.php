<?php

class Techies_Productimportexport_ImportController extends Mage_Adminhtml_Controller_Action {

    public function indexAction() {
        $this->loadLayout()->renderLayout();
    }

    public function postAction() {
        $importproduct = Mage::getModel('techies_productimportexport/importproduct');
        //$post = $this->getRequest()->getPost();
        $fileName = '';
        //print_r($_FILES);
        if (isset($_FILES['product_file']['name']) && $_FILES['product_file']['name'] != '') {
            try {
                // upload csv to folder and check type
                $uploader = new Varien_File_Uploader('product_file');
                $uploader->setAllowedExtensions(array('csv'));
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(false);
                $path = $importproduct->getCSVDirectory();
                $uploader->save($path, $_FILES['product_file']['name']);
                $fileName = $path.$uploader->getUploadedFileName();
                // save file path into database
                $importproduct->setData(array('file'=>$fileName,'status'=>1));
                $importproduct->save();
                
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('techies_productimportexport')->__('Product price CSV imported successfully and price updatation process will start at 1 AM. If you want to start this process now, then click on "Start Price Update Process" button.'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('techies_productimportexport')->__('There is somthing wrong. Please try again'));
                $this->_redirect('*/*/');
            }
        }
        
        $this->_redirect('*/*/');
    }
    public function runCronAction() {
        $jobCode = 'techies_productimportexport_importproduct';
        $email = Mage::getStoreConfig('techies_productimportexport/email/recipient_email');
        try {
//            $scheduleDelete = Mage::getModel('cron/schedule')->getCollection()->addFieldToFilter('job_code',array('eq'=>$jobCode));
            //delete all pending jobs
            $scheduleDelete = Mage::getModel('cron/schedule')->getCollection()->addFieldToFilter('executed_at', array('null' => true))->addFieldToFilter('job_code',array('neq'=>'core_email_queue_send_all'));
            foreach ($scheduleDelete as $item) {
                $item->delete();
            }

            $scheduleDelete = Mage::getModel('cron/schedule')->getCollection()->addFieldToFilter('job_code',array('eq'=>$jobCode))
                    ->addFieldToFilter('executed_at',array('notnull' => true))
                    ->addFieldToFilter('finished_at',array('null' => true));
            if($scheduleDelete->getSize()){
                $message = 'A product price updation is already in progress. Your file is in queue. On successfull completion, you will receive an email at (' . $email . '). It may take from 5 min to 50 min according to number of products.';
//                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('techies_productimportexport')->__('Product price updation is already in progress. On successfull completion, you will receive an email at (' . $email . '). It may take from 5 min to 50 min according to number of products.'));
//                $this->_redirect('*/*/'); exit;
            }else{
                $message = 'Product price update function successfully setlled. On successfull completion, you will receive an email at (' . $email . '). It may take from 5 min to 50 min according to number of products.';
            }
            // set cron schedule
            $importproduct = Mage::getModel('techies_productimportexport/importproduct');
            $importproduct->setImportCronSchedule($jobCode);
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('techies_productimportexport')->__($message));

        } catch (Exception $e) {
            throw new Exception(Mage::helper('cron')->__('Unable to save Cron expression. Please try agian'));
        }
        $this->_redirect('*/*/');
    }

}
