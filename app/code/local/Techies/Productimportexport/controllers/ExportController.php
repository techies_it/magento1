<?php

class Techies_Productimportexport_ExportController extends Mage_Adminhtml_Controller_Action {

    public function indexAction() {
        $this->loadLayout()->renderLayout();
    }
    public function postAction() {
        $post = $this->getRequest()->getPost();
        $import = Mage::getModel('techies_productimportexport/exportproduct');
        $import->export($post);
        exit;
    }
}
