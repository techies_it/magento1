<?php

class Techies_Singlefeed_Model_Exportgoogle extends Mage_Core_Model_Abstract {

    protected $products = array();
    protected $filename;
    protected $csv;
    protected $currencyCode = 'USD';
    protected $storeId = 1;// for cajungrocer store only
    public function export() {
        $this->getProducts();
        $this->prepareCSV();
        $this->saveCSV();
        $this->forceDownload();

        return;
    }

    protected function getProducts() {
        $store = Mage::app()->getStore($this->storeId);
        $products = Mage::getModel('catalog/product')->getCollection()->addStoreFilter($store);
        $products->addAttributeToFilter('status', array('eq' => 1));
        $products->addAttributeToFilter('name', array('notnull' => true))->addAttributeToFilter('name', array('neq' => ''));
        $products->addAttributeToFilter('price', array('gt' => 0));
        //$products->addAttributeToFilter('weight', array('gt' => 0));
        $products->addAttributeToFilter('manufacturer', array('notnull' => true))->addAttributeToFilter('manufacturer', array('neq' => ''));
        $products->addAttributeToSelect('*');
        $products->addAttributeToSort('name', 'ASC')->load();
        $this->products = $products;
        return;
    }

    protected function setHeaders() {
        $this->csv = '"id","title","description","google_product_category","product_type","link","image_link","condition","availability","price","sale_price","sale_price_effective_date","gtin","brand","mpn","shipping_weight"';
        //$this->csv = '"MPN (Manufacturer Part Number)","UPC (Universal Product Code)","UNIQUE INTERNAL CODE","PRODUCT NAME","PRODUCT DESCRIPTION","PRODUCT PRICE","PRODUCT URL","IMAGE URL","CATEGORY","MANUFACTURER","STOCK STATUS","CONDITION (IF NOT NEW)","PAYMENT TYPE","KEYWORDS","MSRP","SALE PRICE","SHIPPING COST","SHIPPING WEIGHT","PROMOTIONAL MESSAGE","GENDER","SIZE","COLOR","AGE RANGE","CURRENCY","material","GOOGLE SHIPPING","Quantity","GOOGLE EXPIRATION DATE","GOOGLE EXPIRATION DATE AUTO RENEW","Become Category","Nextag Category","Pricegrabber Category","Pronto Category","Shopping.com Category","Shopzilla Category","Smarter Category","Sortprice Category","Like.com Category","Shop.com Category","Google Category","Bing Shopping Category","Amazon Product Ads Category","FEED_PROVIDER","online_only","PRODUCT TYPE","Amazon MP Image Type","Amazon Parentage","Item Group ID","Pattern","Google Age Group","ShareASale Category","ShareASale Subcategory","Pricegrabber Bid Type","include cashback","include yahoo! shopping","Include Rocket Fuel","Include LinkConnector","Include Price.com","Include Pepperjam","Include OneWayShopping","Include ShareASale","Include Catalogs.com","Include Find Gift","Include Amazon Marketplace","Include Shopmed","Include StreetPrices","Include Shopzilla 2","Include Shopsavvy","Include Coffee Table","Include Buysight","Include Cognitive Match","Include Catalog On Demand","Include Catalog Spree","Include Go Shopping","Include MyBuys","Include mShopper","Include Gifts.com","Include SearchSpring","Include Commission Junction","INCLUDE GOOGLE BASE","Include Amazon","Include Become","Include buySAFE","Include Like.com","Include NexTag","Include PriceGrabber","Include Pronto","Include Shop.com","Include Shopping.com","Include Shopzilla","Include Smarter","Include Sortprice","INCLUDE THEFIND","INCLUDE POWERREVIEW","Include Bing Shopping","Status","Error Description"';
        return;
    }
    protected function getCurrencyCode(){
        $this->currencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
        return;
    }

    protected function prepareCSV() {
        $this->setHeaders();
        //$base=Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        $base = Mage::app()->getStore($this->storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        $count = 0;
        foreach ($this->products as $product) {
                //$urlProduct = $product->getProductUrl();
                //$urlProduct = $this->getProuductUrlRewriten($product->getUrlPath());
                $urlProduct = $base.$product->getUrlPath();
                // check if product is visible or not and then set url of parent product
                if(!$product->isVisibleInSiteVisibility()){
                    $obj = Mage::getModel('catalog/product'); // Get Object
                    $parentIds = Mage::getResourceSingleton('catalog/product_type_configurable')->getParentIdsByChild($product->getId()); // Get Configurable Parent
                    $parentID = isset($parentIds[0])?$parentIds[0]:null; // Get ID
                    if($parentID){
                        $_parentproduct = $obj->setStoreId($this->storeId)->load($parentID);
                        $attributes = $_parentproduct->getTypeInstance(true)->getConfigurableAttributesAsArray($_parentproduct);
                        $optionArray = array();
                        foreach ($attributes as $attribute){
                            //this will get you the id of the option and value
                            $optionArray[] = $attribute['attribute_id'].'='.$product->getData($attribute['attribute_code']);
                        }
                        $optionLink = implode('&', $optionArray);
                        $urlProduct = $base.$_parentproduct->getUrlPath().'#'.$optionLink;
                    }
                }
                // end of check if product is visible or not and then set url of parent product

                $imagePath = $product->getImageUrl();
                $manufacturer = $product->getAttributeText('manufacturer');
                // check product stock
                $stock_item = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId());
                if($stock_item->getId() && $stock_item->getIsInStock()==0){
                    $productStatus = "out of stock";
                }else{
                    $productStatus = "in stock";
                }
                // end of check produc stock

                $productName = str_replace('"', '', strip_tags($product->getName()));
                $description = str_replace('"', '', strip_tags($product->getDescription()));
                $description = str_replace('&nbsp;', " ", $description);
                if($description==''){
                    $description = $productName;
                }

                $this->csv.=chr(10);
                //'"id","title","description","google_product_category","product_type","link","image_link","condition"'
                $this->csv.='"' . $product->getId() . '","'. $productName .'","' . $description . '","Food, Beverages & Tobacco > Food Items","gourmet food","' . $urlProduct . '","'.$imagePath.'","new"';

                //',"availability","price","sale_price","sale_price_effective_date"'
                $this->csv.=',"'.$productStatus.'","'.number_format($product->getPrice(),2).' '.$this->currencyCode.'","",""';
                //$gtin = 'FALSE';

                //',"gtin","brand","mpn","shipping_weight"'
                $this->csv.=',"","'.$manufacturer.'","'.$product->getSku().'","'.ceil($product->getWeight()).' lb"';

                $count++;
        }
        return;
    }

    protected function saveCSV() {
        $this->filename = $filename = 'techies_googleFeedExport_' . date('jnY-His') . '.csv';
        file_put_contents($this->getCSVDirectory() . $filename, $this->csv);
        return;
    }

    protected function forceDownload() {
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename=' . $this->filename);
        header('Pragma: no-cache');
        readfile($this->getCSVDirectory() . $this->filename);
        exit;
        //return;
    }

    /*     * ** Config Functions *** */

    protected function getCSVDirectory() {
        if (!file_exists(Mage::getBaseDir('base') . DIRECTORY_SEPARATOR . Mage::getStoreConfig('techies_singlefeed_options/configuration/singlefeedDir', Mage::app()->getStore())))
            mkdir(Mage::getBaseDir('base') . DIRECTORY_SEPARATOR . Mage::getStoreConfig('techies_singlefeed_options/configuration/singlefeedDir', Mage::app()->getStore()));
        return Mage::getBaseDir('base') . DIRECTORY_SEPARATOR . Mage::getStoreConfig('techies_singlefeed_options/configuration/singlefeedDir', Mage::app()->getStore());
    }
    protected function getProuductUrlRewriten($urlPath){
        //$oProduct = Mage::getModel('catalog/product')->load($productId);
        $oRewrite = Mage::getModel('core/url_rewrite')->loadByRequestPath($urlPath);
        return $oRewrite->getTargetPath();
    }
}
